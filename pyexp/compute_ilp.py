import gurobipy as gp
import numpy as np
from gurobipy import GRB, Model, tupledict

from eval import get_stc_stats_2
from stc_consts import weak, strong
from utils import load_aggregated_graph

TIMELIMIT = 60 * 60 * 12
LOGFILE = 'gurobilog.txt'


def get_result_graph(G, vals):
    result = G.copy()

    num_strong = 0
    num_weak = 0
    num_other = 0

    epsilon = 0.000001

    for val in vals:
        u = val[0]
        v = val[1]
        label = vals[val]
        if not G.has_edge(u, v): continue
        result.edges[u, v]['label'] = label
        if strong - epsilon <= label <= strong + epsilon:
            num_strong += 1
            result.edges[u, v]['label'] = strong
        elif weak - epsilon <= label <= weak + epsilon:
            num_weak += 1
            result.edges[u, v]['label'] = weak
        else:
            num_other += 1
            print(label)

    print(num_strong, num_weak, num_other)

    return result


def solve_weighted_ilp2(G):
    m = Model("stc")

    m.setParam('TimeLimit', TIMELIMIT)
    m.setParam('LogFile', LOGFILE)

    vars = tupledict()
    objs = tupledict()

    tuples = []
    for (i, j, d) in G.edges(data=True):
        vars[i, j] = m.addVar(vtype=GRB.BINARY, name='e[%d,%d]' % (i, j))
        tuples.append((i, j))
        tuples.append((j, i))
        objs[i, j] = d['weight']

    for i, j in vars.keys():
        vars[j, i] = vars[i, j]
        objs[j, i] = objs[i, j]

    nodes = list(G.nodes)
    for i in nodes:
        ne = [n for n in G.neighbors(i)]
        for j in ne:
            ne2 = [n for n in G.neighbors(j)]
            for k in ne2:
                if i == j or i == k or j == k: continue
                if G.has_edge(i, k): continue
                if (i, j) in vars and (i, k) in vars:
                    m.addConstr(vars[i, j] + vars[i, k] <= 1)
                if (j, i) in vars and (j, k) in vars:
                    m.addConstr(vars[j, i] + vars[j, k] <= 1)

    m.setObjective(gp.quicksum(vars[i, j] * objs[i, j] for (i, j) in tuples), GRB.MAXIMIZE)

    m._vars = vars

    m.optimize()

    vals = m.getAttr('X', vars)

    return get_result_graph(G, vals)


def solve_non_weighted_ilp2(G):
    m = Model("stc")

    m.setParam('TimeLimit', TIMELIMIT)
    m.setParam('LogFile', LOGFILE)

    vars = tupledict()

    tuples = []
    for (i, j, d) in G.edges(data=True):
        vars[i, j] = m.addVar(vtype=GRB.BINARY, name='e[%d,%d]' % (i, j))
        tuples.append((i, j))
        tuples.append((j, i))

    for i, j in vars.keys():
        vars[j, i] = vars[i, j]

    nodes = list(G.nodes)
    for i in nodes:
        ne = [n for n in G.neighbors(i)]
        for j in ne:
            ne2 = [n for n in G.neighbors(j)]
            for k in ne2:
                if i == j or i == k or j == k: continue
                if G.has_edge(i, k): continue
                if (i, j) in vars and (i, k) in vars:
                    m.addConstr(vars[i, j] + vars[i, k] <= 1)
                if (j, i) in vars and (j, k) in vars:
                    m.addConstr(vars[j, i] + vars[j, k] <= 1)

    m.setObjective(gp.quicksum(vars[i, j] for (i, j) in tuples), GRB.MAXIMIZE)

    m._vars = vars

    m.optimize()

    vals = m.getAttr('X', vars)

    return get_result_graph(G, vals)


def run_experiment(dataset):
    print("running ILP experiment: ", dataset)
    ag = 'ag'
    csv_file_name = dataset + "_results"
    csv_file_name += "_exact.csv"

    A = load_aggregated_graph('../datasets/' + dataset + '.tg2.' + ag)
    result = solve_weighted_ilp2(A.copy())
    nonweighted_result = solve_non_weighted_ilp2(A.copy())
    get_stc_stats_2(result, nonweighted_result, csv_file_name)


if __name__ == '__main__':
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
    datasets = ['malawi', 'copresence', 'primary', 'enron-rm', 'yahoo']

    for ds in datasets:
        run_experiment(ds)
