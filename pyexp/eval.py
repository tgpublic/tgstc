import pandas as pd


def get_stc_stats_2(WG, UG, csv_file_name):
    print("#nodes ", WG.number_of_nodes())
    print("#edges ", WG.number_of_edges())

    r = []

    total_weight = 0
    total_weight_a = 0
    total_weight_b = 0
    for (u, v, d) in WG.edges(data=True):
        ul = UG.edges[u, v]["label"]
        total_weight += d['weight']
        if d["label"] == 1:
            total_weight_a += d['weight']
        if ul == 1:
            total_weight_b += d['weight']
        r.append((u, v, d['weight'], int(d['label']), int(ul)))

    df = pd.DataFrame(r,
                      columns=['u', 'v', 'weight', 'w_label', 'uw_label'])

    df.to_csv(csv_file_name)

    print('labels weighted\n', df['w_label'].value_counts())

    print('labels unweighted\n', df['uw_label'].value_counts())

    a = df['w_label'].value_counts()
    print(100 * a[1] / WG.number_of_edges())

    a = df['uw_label'].value_counts()
    print(100 * a[1] / UG.number_of_edges())

    print(df.groupby("w_label")["weight"].agg(["mean", "std"]))
    print(df.groupby("uw_label")["weight"].agg(["mean", "std"]))

    print(100 * total_weight_a / total_weight)
    print(100 * total_weight_b / total_weight)

    print('total weights')
    print(df.groupby('w_label')['weight'].sum())
    print(df.groupby('uw_label')['weight'].sum())
    print()

