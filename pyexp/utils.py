import networkx as nx

from stc_consts import strong


def readFile(filename):
    with open(filename) as f:
        content = f.readlines()
    data = [x.strip() for x in content]
    return data


def load_aggregated_graph(filename):
    data = readFile(filename)
    G = nx.Graph()
    for d in data:
        s = d.split(' ')
        if len(s) < 3: continue
        u = int(s[0])
        v = int(s[1])
        w = float(s[2])
        G.add_edge(u, v, weight=w, label=strong)

    return G
