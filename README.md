# Inferring Tie Strength in Temporal Networks
Source code for our paper [*"Inferring Tie Strength in Temporal Networks"*](https://arxiv.org/abs/2206.11705).


## Build

In the folder `tg_stc` run:
```
mkdir cmake-build-release
cd cmake-build-release
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

After compilation running `tgstc` shows options and help.

To compute the vertex covers in the wedge graphs run `tgstc <filename> -m=1` 
to obtain the aggregated and wedge graphs.
Next run `tgstc <filename>.ag -m=2` to compute the VC.

Run `tgstc <filename> -m=3` for DynAppr, `tgstc <filename> -m=4` for STCtime, `tgstc <filename> -m=5` for DynAppr+, and  `tgstc <filename> -m=6` for STCtime+.

## Data Sets
 
The folder `datasets` contains the smaller data sets in a preprocessed format.
The files are compressed using zip. Please refer to the paper for the relevant references and original sources of the data sets.

The format for the temporal graphs is the following: 
First line states number of nodes. Each following line of 
the format `u v t 1` represents a temporal edge (u,v,t) from node u to v at
Self-loops are removed or will be removed when loading the data sets.

The StackOverflow and Reddit data sets are not stored in the git repository due to their filesize.
They are available at https://snap.stanford.edu/data/sx-stackoverflow.html and https://www.cs.cornell.edu/~arb/data/temporal-reddit-reply/.


## Exact computation using ILP

In the folder `pyexp` is our python code to solve the ILP formulations using 
Gurobi. You need to install Gurobi and add it to your python environment.
First use the `tgstc` program to compute the aggregated `.ag` files of the data sets.

Then run `python3 compute_ilp.py`.

