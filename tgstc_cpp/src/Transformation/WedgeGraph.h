#ifndef CPPSOURCE_WEDGEGRAPH_H
#define CPPSOURCE_WEDGEGRAPH_H

#include <map>
#include <memory>
#include <fstream>
#include "../Temporalgraph/TemporalGraphs.h"
#include <set>

std::pair<NodeId, NodeId> order(NodeId u, NodeId v);


struct WEdge {
    NodeId u{};
    NodeId v{};
};

struct WNode {
    NodeId id;
    double weight = 0;
    std::pair<NodeId, NodeId> e;
    std::vector<WEdge> adjlist;
    std::unordered_set<NodeId> neighbors;

//    double price_sum = 0;
//    bool isTight() const {return price_sum == weight;}
};

struct WedgeGraph {

    void save(std::string const &filename);

    void load(std::string const &filename);

//    void addEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void removeEdge(NodeId u, NodeId v);

    std::vector<WNode> nodes;

    std::vector<WEdge> edges;

    std::map<std::pair<NodeId,NodeId>, uint> edgeNodeMap;

//    uint edge_counter = 1;
//    uint edge_index = 0;

    unsigned long num_edges{};

};


struct WNodeDynamic {
    NodeId id;
    double weight = 0;
    std::pair<NodeId, NodeId> e;
    std::unordered_set<NodeId> neighbors;

    double price_sum = 0;
    bool isTight() const {return price_sum == weight;}
};

struct WedgeGraphDynamic {

    void insertVertex(std::pair<NodeId, NodeId> &wu);

    void deleteVertex(std::pair<NodeId, NodeId> &wu);

    // void deleteIsolatedVertex(std::pair<NodeId, NodeId> &wu);

    void addEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void removeEdge(NodeId u, NodeId v);


    std::vector<WEdge> edges;

    std::map<std::pair<NodeId,NodeId>, uint> edgeNodeMap;

    std::vector<uint> unused_ids;

    uint edge_index = 0;

    unsigned long num_edges{};

    std::vector<WNodeDynamic> nodes;

};


#endif //CPPSOURCE_WEDGEGRAPH_H
