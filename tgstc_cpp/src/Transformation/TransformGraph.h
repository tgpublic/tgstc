#ifndef TGPR_WALKCENTRALITYDLG_H
#define TGPR_WALKCENTRALITYDLG_H

#include <map>
#include <memory>
#include <fstream>
#include "../Temporalgraph/TemporalGraphs.h"
#include "WedgeGraph.h"
#include <set>


struct Node {
    NodeId id;
    std::map<std::pair<uint, uint>, double> edges;
    std::set<NodeId> neighbors;
};

struct Graph {
    explicit Graph(uint num_nodes) {
        nodes.resize(num_nodes);
        for (uint i = 0; i < num_nodes; ++i) {
            nodes[i].id = i;
        }
    }

    std::vector<Node> nodes;

    void insertEdge(NodeId u, NodeId v) {
        if (u < v)
            nodes[u].edges[{u,v}] += 1;
        else
            nodes[v].edges[{v,u}] += 1;

        nodes[u].neighbors.insert(v);
        nodes[v].neighbors.insert(u);
    }

    void insertEdge(NodeId u, NodeId v, double w) {
        insertEdge(u, v);
        if (u < v)
            nodes[u].edges[{u,v}] = w;
        else
            nodes[v].edges[{v,u}] = w;
        num_edges++;
    }

    void addWeight(NodeId u, NodeId v, double d) {
        if (u < v)
            nodes[u].edges[{u,v}] += d;
        else
            nodes[v].edges[{v,u}] += d;
    }

    double getWeight(NodeId u, NodeId v) {
        if (u < v)
            return nodes[u].edges[{u,v}];
        else
            return nodes[v].edges[{v,u}];
    }

    void removeEdge(NodeId u, NodeId v) {
        if (u < v)
            nodes[u].edges.erase({u,v});
        else
            nodes[u].edges.erase({v,u});

        nodes[u].neighbors.erase(v);
        nodes[v].neighbors.erase(u);
        num_edges--;
    }

    void save(const std::string &path) {
        std::ofstream fs;
        fs.open(path);
        if (!fs.is_open()) {
            std::cout << "Could not write data to " << path << std::endl;
            return;
        }

        for (auto &n : nodes) {
            for (auto &e : n.edges) {
                fs << e.first.first << " " << e.first.second << " " << e.second << std::endl;
            }
        }

        fs.close();
    }

    ulong num_edges = 0;

    bool are_neighbors(NodeId a, NodeId b) const {
        return nodes[a].neighbors.find(b) != nodes[a].neighbors.end();
    }

    bool has_edge(NodeId a, NodeId b) const {
        return are_neighbors(a, b) || are_neighbors(b, a);
    }

    void load(const std::string &filepath);
};


Graph temporalGraphToGraph(TemporalGraphStream const &tgs);
Graph temporalGraphToGraphJaccardWeighting(TemporalGraphStream const &tgs);
Graph temporalGraphToGraphExponentialWeighting(TemporalGraphStream const &tgs);

WedgeGraph graphToWedgeGraph(Graph const &g);


std::pair<ulong, ulong> count_wedges_triangles(Graph const &g);


#endif //TGPR_WALKCENTRALITYDLG_H
