//
// Created by lutz on 10.11.22.
//

#ifndef CPPSOURCE_WEDGEHYPERGRAPH_H
#define CPPSOURCE_WEDGEHYPERGRAPH_H


#include <cassert>

#include "Temporalgraph/TemporalGraphs.h"
#include "WedgeGraph.h"

struct WHyperEdge {
    NodeId u{};
    NodeId v{};
    NodeId w{};

    bool operator==(const WHyperEdge& other) const {
        return u == other.u && v == other.v && w == other.w;
    }
};

WHyperEdge order(NodeId u, NodeId v, NodeId w);


struct WHyperEdgeHash {
    size_t operator()(const WHyperEdge& edge) const {
        size_t h1 = std::hash<int64_t>()(edge.u);
        size_t h2 = std::hash<int64_t>()(edge.v);
        size_t h3 = std::hash<int64_t>()(edge.w);

        // Combine hashes using a prime-based method
        return h1 ^ (h2 * 31) ^ (h3 * 61);
    }
};

bool operator<(const WHyperEdge& l, const WHyperEdge& r);

struct WHyperNodeDynamic {

    NodeId id;
    double weight = 0;
    std::pair<NodeId, NodeId> e;
    std::set<std::pair<NodeId, NodeId>> neighbors;

    double price_sum = 0;
     [[nodiscard]] bool isTight() const {
         // assert(weight>=0);
         // assert(price_sum>=0);
//         std::cout << price_sum << " " << weight << "\n";
         auto const d = weight - price_sum;
         return std::abs(d) < 1e-10;
        // return price_sum == weight;
    }

    bool is_virtual = false;

};

struct WedgeHyperGraphDynamic {

    void insertVertex(std::pair<NodeId, NodeId> &wu, bool is_virtual);

    void deleteVertex(std::pair<NodeId, NodeId> &wu);

    // void deleteIsolatedVertex(std::pair<NodeId, NodeId> &wu);

    void addEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv, std::pair<NodeId, NodeId> v_edge);

    void removeEdge(NodeId u, NodeId v, NodeId w);

    // bool nodeExists(const NodeId u) {
        // return nodeIds.find(u) != nodeIds.end();
    // };

    std::vector<WHyperEdge> edges;

    std::map<std::pair<NodeId,NodeId>, NodeId> edgeNodeMap;

    std::vector<uint> unused_ids;

    uint edge_index = 0;

    unsigned long num_edges{};
    // unsigned long num_nodes{};

    std::vector<WHyperNodeDynamic> nodes;

    double alpha = 1.0;

    // std::map<NodeId, uint> nodeDegree;

};

struct WHyperNode {
    NodeId id;
    double weight = 0;
    std::pair<NodeId, NodeId> e;
//    std::vector<WHyperEdge> adjlist;
    std::set<std::pair<NodeId, NodeId>> neighbors;
    bool is_virtual;
};

struct WedgeHyperGraph {

    void load(std::string const &filename, double alpha);

//    void addEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    std::vector<WHyperNode> nodes;

    std::set<WHyperEdge> edges;

    std::map<std::pair<NodeId,NodeId>, uint> edgeNodeMap;

    unsigned long num_edges{};

    void removeEdge(NodeId u, NodeId v, NodeId w);

    void removeIncidentEdges(NodeId u);
};

#endif //CPPSOURCE_WEDGEHYPERGRAPH_H
