#include "WedgeGraph.h"
#include "../Utils/HelperFunctions.h"

using namespace std;

//void WedgeGraph::addEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv) {
//    WEdge we;
//
//    auto u1 = edgeNodeMap[wu];
//    auto v1 = edgeNodeMap[wv];
//
//    if (u1 > v1) {
//        auto t = u1;
//        u1 = v1;
//        v1 = t;
//    }
//
//    we.u = u1;
//    we.v = v1;
//
//    nodes[we.u].adjlist.push_back(we);
//    num_edges++;
//    nodes[we.u].neighbors.insert(we.v);
//    nodes[we.v].neighbors.insert(we.u);
//}


void WedgeGraph::save(const string &path) {
    std::ofstream fs;
    fs.open(path);
    if (!fs.is_open()) {
        std::cout << "Could not write data to " << path << std::endl;
        return;
    }

    for (auto &n : nodes) {
        fs << n.id << " " << n.e.first << " " << n.e.second << " " << n.weight << std::endl;
    }
    fs << std::endl;
    for (auto &n : nodes) {
        for (auto &e : n.adjlist) {
            fs << e.u << " " << e.v << std::endl;
        }
    }

    fs.close();

}


void WedgeGraph::load(const string &path) {
    ifstream fs;
    HF::getFileStream(path, fs);
    string line;
    vector<string> nodes_strs;
    vector<string> edges_strs;

    bool read_nodes = true;
    while (getline(fs, line)) {
        if (line.empty()) {
            read_nodes = false;
            continue;
        }
        if (read_nodes) {
            nodes_strs.push_back(line);
        } else {
            edges_strs.push_back(line);
        }

    }
    fs.close();

    nodes.clear();
    edges.clear();
    num_edges = 0;

    for (auto &s : nodes_strs) {
        auto l = HF::split_string_str(s, ' ');
        WNode wn;
        wn.id = stoul(l[0]);
        wn.e = {stoul(l[1]), stoul(l[2])};
        wn.weight = stod(l[3]);
        nodes.push_back(wn);
    }

    for (auto &s : edges_strs) {
        auto l = HF::split_string(s, ' ');
        WEdge we;
        we.u = l[0];
        we.v = l[1];
        edges.push_back(we);
        nodes[we.u].adjlist.push_back(we);
        num_edges++;
        nodes[we.u].neighbors.insert(we.v);
        nodes[we.v].neighbors.insert(we.u);
    }
}


void WedgeGraph::removeEdge(NodeId u, NodeId v) {
    if (u > v) swap(u, v);
    auto it = nodes[u].adjlist.begin();
    while (it != nodes[u].adjlist.end()) {
        if ((*it).u == u && (*it).v == v) {
            nodes[u].adjlist.erase(it);
            break;
        } else {
            ++it;
        }
    }
    nodes[u].neighbors.erase(v);
    nodes[v].neighbors.erase(u);
    num_edges--;
}



///////////////////////////////////////////////////////////////////////////

void WedgeGraphDynamic::insertVertex(std::pair<NodeId, NodeId> &wu) {
    if (edgeNodeMap.find(wu) == edgeNodeMap.end()) {
        if (unused_ids.empty()){
            edgeNodeMap[wu] = edge_index++;
            nodes.emplace_back();
            nodes.back().e = wu;
            nodes.back().id = edge_index-1;
            nodes.back().weight = 0;
        } else {
            auto nid = unused_ids.back();
            unused_ids.pop_back();
            edgeNodeMap[wu] = nid;
            nodes[nid].e = wu;
            nodes[nid].id = nid;
            nodes[nid].weight = 0;
        }
    }
}

void WedgeGraphDynamic::deleteVertex(std::pair<NodeId, NodeId> &wu) {
    if (edgeNodeMap.find(wu) != edgeNodeMap.end()) {
        auto nid = edgeNodeMap[wu];
        unused_ids.push_back(nid);
        edgeNodeMap.erase(wu);
        nodes[nid].neighbors.clear();
    }
}

// void WedgeGraphDynamic::deleteIsolatedVertex(std::pair<NodeId, NodeId> &wu) {
//     auto nid = edgeNodeMap[wu];
//     if (nodes[nid].neighbors.empty()) {
//         deleteVertex(wu);
//     }
// }

void WedgeGraphDynamic::addEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv) {
    WEdge we;

    insertVertex(wu);
    insertVertex(wv);

    auto u1 = edgeNodeMap[wu];
    auto v1 = edgeNodeMap[wv];

    if (u1 > v1) {
        auto t = u1;
        u1 = v1;
        v1 = t;
    }

    we.u = u1;
    we.v = v1;

//    nodes[we.u].adjlist.push_back(we);
    num_edges++;
    nodes[we.u].neighbors.insert(we.v);
    nodes[we.v].neighbors.insert(we.u);
}




void WedgeGraphDynamic::removeEdge(NodeId u, NodeId v) {
    if (u > v) swap(u, v);
    nodes[u].neighbors.erase(v);
    nodes[v].neighbors.erase(u);
    num_edges--;
}


