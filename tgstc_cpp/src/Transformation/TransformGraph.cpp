#include <cassert>
#include <valarray>
#include "TransformGraph.h"
#include "../Utils/HelperFunctions.h"

using namespace std;


std::pair<NodeId, NodeId> order(NodeId u, NodeId v){
    if (u < v) return {u, v};
    return {v, u};
}


bool is_wedge(Graph const &g, NodeId u, NodeId v, NodeId w) {
    // center node v
    auto a = g.are_neighbors(u, v);
    auto b = g.are_neighbors(v, w);
    auto c = g.are_neighbors(w, u);
    return !(a && b && c);
}


WedgeGraph graphToWedgeGraph(Graph const &g) {
    WedgeGraph w;

    uint i = 0;
    for (auto &n : g.nodes) {
        for (auto &e : n.edges) {
            WNode wn;
            wn.id = i++;
            wn.e = e.first;
            wn.weight = e.second;
            w.nodes.push_back(wn);
            w.edgeNodeMap.insert({e.first, wn.id});
            w.edgeNodeMap.insert({{e.first.second, e.first.first}, wn.id});
        }
    }

    for (auto &n : g.nodes) {
        auto u = n.id;
        for (auto v : n.neighbors) {
            for (auto x : n.neighbors) {
                if (v >= x) continue;
                if (u == v || u == x) continue;
                if (!g.are_neighbors(v, x)) {

                    assert(w.edgeNodeMap.at({u,v})>=0);
                    auto u1 = w.edgeNodeMap[{u,v}];
                    auto v1 = w.edgeNodeMap[{u,x}];

                    if (u1 > v1) swap(u1, v1);

                    WEdge we;
                    we.u = u1;
                    we.v = v1;

                    w.nodes[we.u].adjlist.push_back(we);
                    w.num_edges++;
                    w.nodes[we.u].neighbors.insert(we.v);
                    w.nodes[we.v].neighbors.insert(we.u);
                }
            }
        }
    }
    return w;
}

Graph temporalGraphToGraph(TemporalGraphStream const &tgs) {
    Graph g(tgs.num_nodes);

    for (auto &e : tgs.edges) {
        g.insertEdge(e.u_id, e.v_id);
    }

    for (auto &n : g.nodes)
        g.num_edges += n.edges.size();

    return g;
}

Graph temporalGraphToGraphJaccardWeighting(TemporalGraphStream const &tgs) {
    Graph g(tgs.num_nodes);
    map<pair<NodeId, NodeId>, uint> edgecounter;
    vector<uint> degree(tgs.num_nodes, 0);

    for (auto &e : tgs.edges) {
        g.insertEdge(e.u_id, e.v_id);
        edgecounter[{e.u_id, e.v_id}]++;
        edgecounter[{e.v_id, e.u_id}]++;
        degree[e.u_id]++;
        degree[e.v_id]++;
    }

    for (auto &n : g.nodes) {
        g.num_edges += n.edges.size();

        for (auto &e : n.edges) {
            double a = edgecounter[e.first];
            double b = 1.0 * degree[e.first.first] + 1.0 * degree[e.first.second];
            e.second = a / b;
        }
    }
    return g;
}

Graph temporalGraphToGraphExponentialWeighting(TemporalGraphStream const &tgs) {
    Graph g(tgs.num_nodes);
    map<pair<NodeId, NodeId>, vector<Time>> times;
    for (auto &e : tgs.edges) {
        times[{e.u_id, e.v_id}].push_back(e.t);
    }
    for (auto &p : times) {
        double weight = 1;
        auto &e = p.first;
        for (int i = 0; i < p.second.size()-1; ++i) {
            for (int j = i+1; j < p.second.size(); ++j) {
                double t_1 = p.second[i];
                double t_2 = p.second[j];
                weight += 1+t_2-t_1;//exp(-1.0 * (t_2 - t_1));
//                weight += pow(1.3,-1.0 * (t_2 - t_1));
//                g.insertEdge(e.first, e.second);
            }
        }
        g.insertEdge(e.first, e.second, weight);
    }

    for (auto &n : g.nodes)
        g.num_edges += n.edges.size();

    return g;
}


//pair<ulong, ulong> count_wedges_triangles(Graph const &g) {
////    int j = 0;
//    ulong w = 0, t = 0;
//    for (auto &n : g.nodes) {
//        auto u = n.id;
////        cout << j++ << endl;
//        for (auto v : n.neighbors) {
//            for (auto x : n.neighbors) {
//                if (v >= x) continue;
//                if (is_wedge(g, u, v, x)) {
//                    w++;
//                } else {
//                    t++;
//                }
//            }
//        }
//    }
//    assert(t % 3 == 0);
//    return {w, t/3};
//}

pair<ulong, ulong> count_wedges_triangles(Graph const &g) {
    ulong w = 0, t = 0;
    for (auto &n : g.nodes) {
        auto u = n.id;
        for (auto v : n.neighbors) {
            for (auto x : n.neighbors) {
                if (v >= x) continue;
                if (u == v || u == x) continue;
                if (!g.are_neighbors(v, x)) {
                    w++;
                } else {
                    t++;
                }
            }
        }
    }
    return {w, t/3};
}

void Graph::load(const string &filepath) {
    ifstream fs;
    HF::getFileStream(filepath, fs);
    string line;
    vector<string> nodes_strs;
    vector<NodeId> us;
    vector<NodeId> vs;
    vector<double> ws;
    uint num_nodes = 0;

    while (getline(fs, line)) {
        nodes_strs.push_back(line);
        auto vec = HF::split_string_str(line, ' ');
        auto u = stoul(vec[0]);
        auto v = stoul(vec[1]);
        if (u > v) swap(u, v);
        auto w = stod(vec[2]);
        us.push_back(u);
        vs.push_back(v);
        ws.push_back(w);
        if (u > num_nodes) num_nodes = u;
        if (v > num_nodes) num_nodes = v;

    }
    fs.close();
    num_nodes++;
//    nodes.resize(num_nodes);

    for (uint i = 0; i < num_nodes; ++i) {
        Node n;
        n.id = i;
        nodes.push_back(n);
    }

    for (int i = 0; i < us.size(); ++i) {
        auto u = us[i];
        auto v = vs[i];
        auto w = ws[i];
        assert(u <= v);
        nodes[u].edges[{u,v}] = w;
        nodes[u].neighbors.insert(v);
        nodes[v].neighbors.insert(u);
        num_edges++;
    }

}
