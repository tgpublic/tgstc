#include "WedgeHyperGraph.h"
#include "Utils/HelperFunctions.h"

using namespace std;

//void WedgeHyperGraph::load(const string &path, double alpha) {
//    ifstream fs;
//    HF::getFileStream(path, fs);
//    string line;
//    vector<string> nodes_strs;
//    vector<string> edges_strs;
//
//    bool read_nodes = true;
//    while (getline(fs, line)) {
//        if (line.empty()) {
//            read_nodes = false;
//            continue;
//        }
//        if (read_nodes) {
//            nodes_strs.push_back(line);
//        } else {
//            edges_strs.push_back(line);
//        }
//
//    }
//    fs.close();
//
//    nodes.clear();
//    edges.clear();
//    num_edges = 0;
//
//    for (auto &s : nodes_strs) {
//        auto l = HF::split_string_str(s, ' ');
//        WHyperNode wn;
//        wn.id = stoul(l[0]);
//        wn.e = {stoul(l[1]), stoul(l[2])};
//        wn.weight = stod(l[3]);
//        wn.is_virtual = false;
//        nodes.push_back(wn);
//    }
//
//    map<pair<NodeId, NodeId>, NodeId> vedges_nid_map;
//    map<NodeId, uint> wedges_vedge;
//
//    for (auto &s : edges_strs) {
//        auto l = HF::split_string(s, ' ');
//
//        auto u = nodes[l[0]].e;
//        auto u_id = nodes[l[0]].id;
//        auto v = nodes[l[1]].e;
//        auto v_id = nodes[l[1]].id;
//        pair<NodeId, NodeId> v_edge_node;
//        if (u.first == v.first) {
//            v_edge_node = order(u.second, v.second);
//        } else if (u.first == v.second) {
//            v_edge_node = order(u.second, v.first);
//        }
//        if (u.second == v.first) {
//            v_edge_node = order(u.first, v.second);
//        } else if (u.second == v.second) {
//            v_edge_node = order(u.first, v.first);
//        }
//        if (vedges_nid_map.find(v_edge_node) == vedges_nid_map.end()) {
//            WHyperNode wn;
//            nodes.push_back(wn);
//            nodes.back().id = nodes.size() - 1;
//            nodes.back().e = v_edge_node;
//            nodes.back().is_virtual = true;
//            nodes.back().weight = (nodes[u_id].weight + nodes[v_id].weight);
//            vedges_nid_map[v_edge_node] = nodes.back().id;
//            wedges_vedge[vedges_nid_map[v_edge_node]] = 1;
//        } else {
//            nodes[vedges_nid_map[v_edge_node]].weight += (nodes[u_id].weight + nodes[v_id].weight);
//            wedges_vedge[vedges_nid_map[v_edge_node]] += 1;
//        }
//
//        WHyperEdge we = order(u_id, v_id, vedges_nid_map[v_edge_node]);
//
//        edges.insert(we);
//        num_edges++;
//        nodes[we.u].neighbors.insert(order(we.v, we.w));
//        nodes[we.v].neighbors.insert(order(we.u, we.w));
//        nodes[we.w].neighbors.insert(order(we.u, we.v));
//    }
//
//    for (auto &p : vedges_nid_map) {
//        nodes[p.second].weight = (nodes[p.second].weight / wedges_vedge[p.second]) * alpha;
//    }
//}


pair<NodeId, NodeId> get_vedge_node(WHyperNode const &u, WHyperNode const &v){
    pair<NodeId, NodeId> v_edge_node;
    if (u.e.first == v.e.first) {
        v_edge_node = order(u.e.second, v.e.second);
    } else if (u.e.first == v.e.second) {
        v_edge_node = order(u.e.second, v.e.first);
    }
    if (u.e.second == v.e.first) {
        v_edge_node = order(u.e.first, v.e.second);
    } else if (u.e.second == v.e.second) {
        v_edge_node = order(u.e.first, v.e.first);
    }
    return v_edge_node;
}


void WedgeHyperGraph::load(const string &path, double alpha) {
    ifstream fs;
    HF::getFileStream(path, fs);
    string line;
    vector<string> nodes_strs;
    vector<string> edges_strs;

    bool read_nodes = true;
    while (getline(fs, line)) {
        if (line.empty()) {
            read_nodes = false;
            continue;
        }
        if (read_nodes) {
            nodes_strs.push_back(line);
        } else {
            edges_strs.push_back(line);
        }

    }
    fs.close();

    nodes.clear();
    edges.clear();
    num_edges = 0;

    set<pair<NodeId, NodeId>> node_set;

    for (auto &s : nodes_strs) {
        auto l = HF::split_string_str(s, ' ');
        WHyperNode wn;
        wn.id = stoul(l[0]);
        wn.e = {stoul(l[1]), stoul(l[2])};
        wn.weight = stod(l[3]);
        wn.is_virtual = false;
        nodes.push_back(wn);

        node_set.insert(order(wn.e.first, wn.e.second));
    }

    map<pair<NodeId, NodeId>, NodeId> vedges_nid_map;
    map<NodeId, uint> wedges_vedge;

    for (auto &s : edges_strs) {
        auto l = HF::split_string(s, ' ');

        auto u_id = nodes[l[0]].id;
        auto v_id = nodes[l[1]].id;
        pair<NodeId, NodeId> v_edge_node = get_vedge_node(nodes[l[0]], nodes[l[1]]);

        if (vedges_nid_map.find(v_edge_node) == vedges_nid_map.end()) {
            WHyperNode wn;
            nodes.push_back(wn);
            nodes.back().id = nodes.size() - 1;
            nodes.back().e = v_edge_node;
            nodes.back().is_virtual = true;
            nodes.back().weight = (nodes[u_id].weight + nodes[v_id].weight);
            vedges_nid_map[v_edge_node] = nodes.back().id;
            wedges_vedge[vedges_nid_map[v_edge_node]] = 1;
        } else {
            nodes[vedges_nid_map[v_edge_node]].weight += (nodes[u_id].weight + nodes[v_id].weight);
            wedges_vedge[vedges_nid_map[v_edge_node]] += 1;
        }

        WHyperEdge we = order(u_id, v_id, vedges_nid_map[v_edge_node]);

        edges.insert(we);
        num_edges++;
        nodes[we.u].neighbors.insert(order(we.v, we.w));
        nodes[we.v].neighbors.insert(order(we.u, we.w));
        nodes[we.w].neighbors.insert(order(we.u, we.v));
    }

    for (auto &p : vedges_nid_map) {
        nodes[p.second].weight = (nodes[p.second].weight / wedges_vedge[p.second]) * alpha;
    }
}


void WedgeHyperGraph::removeEdge(NodeId u, NodeId v, NodeId w) {
    auto we = order(u, v, w);
    nodes[u].neighbors.erase({we.v, we.w});
    nodes[v].neighbors.erase({we.u, we.w});
    nodes[w].neighbors.erase({we.u, we.v});
    edges.erase(WHyperEdge{we.u, we.v, we.w});
    num_edges--;
}

void WedgeHyperGraph::removeIncidentEdges(NodeId u) {
    for (auto &[v, w] : nodes[u].neighbors) {
        edges.erase(order(u, v, w));
        num_edges--;

        nodes[v].neighbors.erase(order(u, w));
        nodes[w].neighbors.erase(order(u, v));
    }
    nodes[u].neighbors.clear();
}


///////////////////////////////////////////////////////////////////////////


void WedgeHyperGraphDynamic::insertVertex(std::pair<NodeId, NodeId> &wu, bool is_virtual) {
    if (edgeNodeMap.find(wu) == edgeNodeMap.end()) {
        if (unused_ids.empty()){
            edgeNodeMap[wu] = edge_index++;
            nodes.emplace_back();
            nodes.back().e = wu;
            nodes.back().id = edge_index-1;
            nodes.back().weight = 0;
            nodes.back().is_virtual = is_virtual;
            // nodeIds.insert(nodes.back().id);
            // nodeDegree[nodes.back().id] = 1;
            // ++num_nodes;
        } else {
            auto nid = unused_ids.back();
            unused_ids.pop_back();
            edgeNodeMap[wu] = nid;
            nodes[nid].e = wu;
            nodes[nid].id = nid;
            nodes[nid].weight = 0;
            nodes[nid].is_virtual = is_virtual;
            // nodeIds.insert(nid);
            // nodeDegree[nid] = 1;
            // ++num_nodes;
        }
    }
    // } else
        // nodeDegree[edgeNodeMap[wu]]++;
}

void WedgeHyperGraphDynamic::deleteVertex(std::pair<NodeId, NodeId> &wu) {
    if (edgeNodeMap.find(wu) != edgeNodeMap.end()) {
        auto nid = edgeNodeMap[wu];
        unused_ids.push_back(nid);
        edgeNodeMap.erase(wu);
        nodes[nid].neighbors.clear();
        // --num_nodes;
    }
}

// void WedgeHyperGraphDynamic::deleteIsolatedVertex(std::pair<NodeId, NodeId> &wu) {
//     auto nid = edgeNodeMap[wu];
//     if (nodes[nid].neighbors.empty()) {
//         deleteVertex(wu);
//     }
// }

void WedgeHyperGraphDynamic::addEdge(std::pair<NodeId, NodeId> wu,
                                     std::pair<NodeId, NodeId> wv, std::pair<NodeId, NodeId> v_edge) {

    insertVertex(wu, false);
    insertVertex(wv, false);
    insertVertex(v_edge, true);

    auto u = edgeNodeMap[wu];
    auto v = edgeNodeMap[wv];
    auto w = edgeNodeMap[v_edge];

    WHyperEdge we = order(u, v, w);

    num_edges++;
    nodes[we.u].neighbors.insert({we.v, we.w});
    nodes[we.v].neighbors.insert({we.u, we.w});
    nodes[we.w].neighbors.insert({we.u, we.v});

    if (nodes[w].weight == 0) nodes[w].weight = alpha;
    // todo
    // for (auto &p : nodes[u].neighbors) {
    //     nodes[w].weight += nodes[p.first].weight;
    //     nodes[w].weight += nodes[p.second].weight;
    // }
    // nodes[w].weight /= alpha * nodes[w].neighbors.size();
}




void WedgeHyperGraphDynamic::removeEdge(const NodeId u, const NodeId v, const NodeId w) {
    const auto [xu, xv, xw] = order(u, v, w);
    nodes[xu].neighbors.erase({xv, xw});
    nodes[xv].neighbors.erase({xu, xw});
    nodes[xw].neighbors.erase({xu, xv});
    num_edges--;
}

bool operator<(const WHyperEdge& l, const WHyperEdge& r) {
    return std::tie(l.u, l.v, l.w) < std::tie(r.u, r.v, r.w);
}


WHyperEdge order(NodeId u, NodeId v, NodeId w) {
    WHyperEdge e;
    if (u > v) std::swap(u, v);
    if (u > w) std::swap(u, w);
    if (v > w) std::swap(v, w);
    e.u = u;
    e.v = v;
    e.w = w;
    return e;
}
