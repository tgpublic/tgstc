#ifndef CPPSOURCE_DYNAMICPRICINGSTREAMING_H
#define CPPSOURCE_DYNAMICPRICINGSTREAMING_H

#include "../Temporalgraph/TemporalGraphs.h"
#include "../Transformation/TransformGraph.h"
#include "../Utils/Params.h"
#include "DynamicPricing.h"
#include <unordered_set>
#include <set>
#include <map>

class DynamicPricingStreaming {

public:

    DynamicPricingStreaming();

    void run_update_sequence(Sigma const &sigma);

#ifdef PRINT_DATA_TO_FILES
    void save_data(std::string const &outfile);
#endif

    Cover C;
    WedgeGraphDynamic W;

private:

    void update(const EdgesV& F);

    void update(const std::pair<NodeId, NodeId> &e);

    void insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void decreaseWeight(std::pair<NodeId, NodeId> u, double d);

    void increaseWeight(std::pair<NodeId, NodeId> u, double d);

//    bool isTight(NodeId u);

    bool inCover(NodeId u);

    void removeFromCover(NodeId u);

    void update_counts();


    Prices prices;
//    std::vector<double> price_sum;

#ifdef PRINT_DATA_TO_FILES
    std::vector<uint> C_sizes;
    std::vector<std::vector<std::pair<NodeId, NodeId>>> C_counts;
    std::vector<uint> C_weights;
    std::vector<uint> W_edges;
    uint num_time_windows = 0;
#endif
};

void run_dynamic_experiment_streaming(const TemporalGraphStream& tgs, Params const &params);

void write_results_to_file(const Params &params, std::vector<double> times,
                           std::vector<uint> num_edges, std::vector<uint> num_nodes,
                            std::vector<uint> C_size);


#endif //CPPSOURCE_DYNAMICPRICINGSTREAMING_H
