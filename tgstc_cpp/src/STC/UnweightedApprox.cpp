#include <cassert>
#include "UnweightedApprox.h"

using namespace std;


NodeId getNextVertex(vector<uint> const &degrees,
                     map<pair<NodeId,NodeId>, uint> const &common_neighbors, WedgeGraph const& w) {
    uint max = 0;
    for (unsigned int degree : degrees) {
        if (degree > max) {
            max = degree;
        }
    }
    if (max == 0) return MAX_UINT_VALUE;

    vector<NodeId> eq;
    for (int i = 0; i < degrees.size(); ++i) {
        if (degrees[i] == max) {
            eq.push_back(i);
        }
    }
    uint xx = MAX_UINT_VALUE, result = eq[0];
    for (auto &r : eq) {
        if (common_neighbors.find(w.nodes[r].e) == common_neighbors.end()) continue;
        if (common_neighbors.at(w.nodes[r].e) < xx) {
            xx = common_neighbors.at(w.nodes[r].e);
            result = r;
        }
    }

    return result;
}


map<pair<NodeId,NodeId>, uint> get_common_neighbors(Graph const& g) {
    map<pair<NodeId,NodeId>, uint> common_neighbors;
    for (auto &u : g.nodes) {
        for (auto &v : u.neighbors) {
            for (auto &x : g.nodes[v].neighbors) {
                if (g.are_neighbors(u.id, x))
                    common_neighbors[{u.id, v}]++;
            }
        }
    }
    return common_neighbors;
}


std::unordered_set<NodeId> unweighted_approx_matching(WedgeGraph const &w, Graph const &g) {
    map<pair<NodeId,NodeId>, uint> common_neighbors = get_common_neighbors(g);

    unordered_set<NodeId> cover;
    vector<uint> degrees(w.nodes.size());
    for (auto &e : w.nodes) {
        degrees[e.id] = e.neighbors.size();
    }

    while (true) {
        auto n = getNextVertex(degrees, common_neighbors, w);
        if (n==MAX_UINT_VALUE) break;

        vector<pair<uint,uint>> ndegs;
        for (auto &v : w.nodes[n].neighbors) {
            ndegs.emplace_back(v,degrees[v]);
        }
        sort(ndegs.begin(), ndegs.end(), [](const auto &a, const auto &b){return a.second > b.second;});
        auto u = ndegs[0].first;

        assert(cover.find(n) == cover.end());
        assert(cover.find(u) == cover.end());

        cover.insert(n);
        cover.insert(u);
        degrees[n] = 0;
        degrees[u] = 0;
        for (auto &v : w.nodes[n].neighbors) {
            if (degrees[v] > 0) degrees[v]--;
        }
        for (auto &v : w.nodes[u].neighbors) {
            if (degrees[v] > 0) degrees[v]--;
        }
    }
    return cover;
}




std::unordered_set<NodeId> unweighted_approx_highdeg(WedgeGraph const &w, Graph const &g) {
    map<pair<NodeId,NodeId>, uint> common_neighbors = get_common_neighbors(g);

    unordered_set<NodeId> cover;
    vector<uint> degrees(w.nodes.size());
    for (auto &e : w.nodes) {
        degrees[e.id] = e.neighbors.size();
    }
    while (true) {
        auto n = getNextVertex(degrees, common_neighbors, w);
        if (n==MAX_UINT_VALUE) break;
        assert(cover.find(n) == cover.end());
        cover.insert(n);
        degrees[n] = 0;
        for (auto &v : w.nodes[n].neighbors) {
            if (degrees[v] > 0) degrees[v]--;
        }
    }

    return cover;
}


