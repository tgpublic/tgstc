#include "DynamicStreamStats.h"
#include "Utils/SGLog.h"
#include "Utils/HelperFunctions.h"

using namespace std;

void write_results_to_file(const Params &params, std::vector<double> &times,
                           std::vector<uint> &num_edges, std::vector<uint> &num_nodes, std::vector<uint> &A_num_edges);

void DynamicStreamStats::run_update_sequence(Sigma const &sigma) {
    if (sigma.empty()) return;
    for (auto &s : sigma) {
        auto u = order(s.wu.first, s.wu.second);
        auto v = order(s.wv.first, s.wv.second);
        switch (s.updateType) {
            case IE:
                insertEdge(u, v);
                break;
            case DE:
                deleteEdge(u, v);
                break;
            case CW:
                if (s.d > 0) increaseWeight(u, s.d);
                else decreaseWeight(u, s.d);
                break;
            case RN:
                auto node = W.edgeNodeMap[order(u.first, u.second)];
                W.nodes[node].weight = 0;
                W.deleteVertex(u);
                break;
        }
    }
}

void DynamicStreamStats::insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv) {
    W.addEdge(wu, wv);
}

void DynamicStreamStats::deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv) {
    NodeId u = W.edgeNodeMap[wu], v = W.edgeNodeMap[wv];
    W.removeEdge(u, v);
}

void DynamicStreamStats::decreaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    auto u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
}

void DynamicStreamStats::increaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    W.insertVertex(wu);
    auto u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
}


void run_dynamic_experiment_stats(const TemporalGraphStream& tgs, Params const &params) {

    auto delta = params.delta;
    auto stepsize = params.stepsize;

    uint pos = params.start_pos;

    Time start = tgs.edges[pos].t;
    Time end = tgs.edges.back().t;

    uint cur_start = start,
            cur_end = start + delta - 1;

    DynamicStreamStats dynamic_MWVC;

//    TemporalEdges edges;
    list<TemporalEdge> edges;

    Graph graph(tgs.num_nodes);

    Timer timer;
    vector<double> times;
    vector<uint> num_edges;
    vector<uint> A_num_edges;
    vector<uint> num_nodes;
    vector<uint> C_size;

    double time = 0;
    double aggr_time = 0;
    uint po = 0;

    while (tgs.edges[pos].t <= end - delta + 1) {


        TemporalEdges new_edges, removed_edges;

        if (!edges.empty()) {
            int a = tgs.edges[pos].t - cur_end;
            int b = edges.front().t - cur_start;
            int c = min(a, b);
            if (c > 0) {
                cur_start += c;
                cur_end += c;
            }
        }

        auto it = edges.begin();
        while (it != edges.end()) {
            if ((*it).t < cur_start) {
                removed_edges.push_back(*it);
                it = edges.erase(it);
            } else {
                break;
            }
        }

        while (tgs.edges[pos].t <= cur_end) {
            if (tgs.edges[pos].u_id != tgs.edges[pos].v_id)
                new_edges.push_back(tgs.edges[pos]);
            pos++;
            if (pos >= tgs.edges.size()) break;
        }

        cur_start += stepsize;
        cur_end = min((ulong)stepsize + cur_end, end);


        if (removed_edges.empty() && new_edges.empty()) {
//            cout << pos << " empty" << endl;
            continue;
        }

        edges.insert(edges.end(), new_edges.begin(), new_edges.end());


        timer.start();
        auto sigma = update_aggregation_and_wedge_graph(new_edges, removed_edges, graph);
        auto t = timer.stop();
        times.push_back(t);

        if (po % 10000 == 0) {
            cout << "delta:     \t" << delta << endl;
            cout << "e.tl - e.t1\t" << edges.back().t - edges.front().t << endl;
            cout << "edges size:\t" << edges.size() << endl;
            cout << "new edges:\t" << new_edges.size() << endl;
            cout << "rem. edges:\t" << removed_edges.size() << endl;
            cout << "Sigma len:\t" << sigma.size() << endl;
            cout << "#A.nodes:\t" << graph.nodes.size() << endl;
            cout << "#A.edges:\t" << graph.num_edges << endl;
            cout << "#W.nodes:\t" << dynamic_MWVC.W.nodes.size() << endl;
            cout << "#W.edges:\t" << dynamic_MWVC.W.num_edges << endl;
        }

        dynamic_MWVC.run_update_sequence(sigma);

        num_edges.push_back(dynamic_MWVC.W.num_edges);
        A_num_edges.push_back(graph.num_edges);
        num_nodes.push_back(dynamic_MWVC.W.nodes.size());

        if (po % 10000 == 0) {
            cout << "Iter:\t" << po << endl;
            cout << "Time:\t" << t << endl;
            cout << "Pos: \t" << pos << " of " << tgs.edges.size() << endl;
            cout << "------------------------------------------" << endl;
        }
        po++;
//        if (po == params.windows_to_process) break;

    }
    write_results_to_file(params, times, num_edges, num_nodes, A_num_edges);
    SGLog::log() << "running time complete: " << time << endl;
    cout << fixed;
    cout << time << endl;

}


void write_results_to_file(const Params &params, std::vector<double> &times,
                           std::vector<uint> &num_edges, std::vector<uint> &num_nodes, std::vector<uint> &A_num_edges) {
    auto times_file = params.dataset_path + "_"
                      + to_string(params.mode) + "_" + to_string(params.delta) + ".times";
    auto num_edges_file = params.dataset_path + "_"
                          + to_string(params.mode) + "_" + to_string(params.delta) + ".edges";
    auto A_num_edges_file = params.dataset_path + "_"
                          + to_string(params.mode) + "_" + to_string(params.delta) + ".aedges";
    auto num_nodes_file = params.dataset_path + "_"
                          + to_string(params.mode) + "_" + to_string(params.delta) + ".nodes";

    HF::writeVectorToFile(times_file, times);
    HF::writeVectorToFile(num_edges_file, num_edges);
    HF::writeVectorToFile(A_num_edges_file, A_num_edges);
    HF::writeVectorToFile(num_nodes_file, num_nodes);
}