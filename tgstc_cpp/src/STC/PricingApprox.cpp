#include "PricingApprox.h"

using namespace std;

unordered_set<NodeId> pricing_approx(WedgeGraph const &w) {

    vector<double> vertex_price_sum(w.nodes.size(), 0);

    unordered_set<NodeId> cover;

    vector<pair<WEdge, uint>> perm;
    for (auto &e : w.edges) {
        auto &u = w.nodes[e.u];
        auto &v = w.nodes[e.v];
        auto weight = u.neighbors.size() + v.neighbors.size();
        perm.emplace_back(e, weight);
    }

    sort(perm.begin(), perm.end(), [](const auto &p1, const auto &p2)
        {return p1.second > p2.second;});

    for (auto &p : perm) {
        auto &e = p.first;
        if (vertex_price_sum[e.u] == w.nodes[e.u].weight || vertex_price_sum[e.v] == w.nodes[e.v].weight) continue;

        auto wu = w.nodes[e.u].weight - vertex_price_sum[e.u];
        auto wv = w.nodes[e.v].weight - vertex_price_sum[e.v];

        double increase = wv;
        if (wu < wv) {
            increase = wu;
        }
        vertex_price_sum[e.u] += increase;
        vertex_price_sum[e.v] += increase;

        if (vertex_price_sum[e.u] == w.nodes[e.u].weight) cover.insert(e.u);
        if (vertex_price_sum[e.v] == w.nodes[e.v].weight) cover.insert(e.v);

    }
    return cover;
}