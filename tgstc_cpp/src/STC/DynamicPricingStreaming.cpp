#include "DynamicPricingStreaming.h"
#include "../Utils/HelperFunctions.h"
#include "Utils/SGLog.h"

#include <utility>
#include <cassert>
#include <numeric>

using namespace std;

void DynamicPricingStreaming::run_update_sequence(Sigma const &sigma) {
    if (sigma.empty()) return;
    for (auto &s : sigma) {
        auto u = order(s.wu.first, s.wu.second);
        auto v = order(s.wv.first, s.wv.second);
        switch (s.updateType) {
            case IE:
                insertEdge(u, v);
                break;
            case DE:
                deleteEdge(u, v);
                break;
            case CW:
                assert(s.d != 0);
                if (s.d > 0) increaseWeight(u, s.d);
                else decreaseWeight(u, s.d);
                break;
            case RN:
                auto node = W.edgeNodeMap[order(u.first, u.second)];
                if (inCover(node)) C.erase(node);
                assert(W.nodes[node].neighbors.empty());
                W.nodes[node].weight = 0;
                W.deleteVertex(u);
                removeFromCover(node);
                break;
        }
    }
//    cout << C.size() << endl;
#ifdef PRINT_DATA_TO_FILES
    update_counts();
#endif
}

void DynamicPricingStreaming::update(const std::pair<NodeId, NodeId> &e) {
    assert(e.first < e.second);
    auto u = e.first, v = e.second;

    if (inCover(u) || inCover(v)) return;

    auto diffu = W.nodes[u].weight - W.nodes[u].price_sum;
    auto diffv = W.nodes[v].weight - W.nodes[v].price_sum;
    auto diff = min(diffu, diffv);

    prices[{u, v}] += diff;
    W.nodes[u].price_sum += diff;
    W.nodes[v].price_sum += diff;

    if (W.nodes[u].isTight()) C.insert(u);
    if (W.nodes[v].isTight()) C.insert(v);
}

void DynamicPricingStreaming::update(const EdgesV & F) {
    for (auto &e : F) {
        update(e);
    }
    assert(verify_cover(W, C));
}

void DynamicPricingStreaming::insertEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv) {
    W.addEdge(wu, wv);
    auto u = W.edgeNodeMap[wu];
    auto v = W.edgeNodeMap[wv];
    auto f = order(u,v);
    assert(prices.find(f) == prices.end());
    prices[f] = 0;
    update(f);
}

void DynamicPricingStreaming::deleteEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv) {
    NodeId u = W.edgeNodeMap[wu], v = W.edgeNodeMap[wv];
    auto p = order(u, v);
    W.removeEdge(p.first, p.second);
    auto price = prices[p];
    prices.erase(p);
    W.nodes[u].price_sum -= price;
    W.nodes[v].price_sum -= price;
    if (!W.nodes[u].isTight())
        removeFromCover(u);
    if (!W.nodes[v].isTight())
        removeFromCover(v);
    EdgesV F;
    for (auto &n : W.nodes[u].neighbors) {
        if (!inCover(n)) {
            F.push_back(order(u,n));
        }
    }
    for (auto &n : W.nodes[v].neighbors) {
        if (!inCover(n)) {
            F.push_back(order(v, n));
        }
    }
//    W.deleteIsolatedVertex(wu);
//    W.deleteIsolatedVertex(wv);
    update(F);
}

void DynamicPricingStreaming::decreaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    auto u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
    if (inCover(u)) removeFromCover(u);
    auto &Fp = W.nodes[u].neighbors;
    EdgesV F;

    for (auto &n : Fp) {
        auto p = prices[order(u, n)];
        prices[order(u, n)] = 0;
        W.nodes[u].price_sum -= p;
        W.nodes[n].price_sum -= p;
        if (!W.nodes[n].isTight()) {
            F.push_back(order(u, n));
            if (inCover(n)) removeFromCover(n);
            auto &nv = W.nodes[n].neighbors;
            for (auto &f : nv) {
                if (!W.nodes[f].isTight()) {
                    F.push_back(order(n, f));
                }
            }
        }
    }
    update(F);
}


void DynamicPricingStreaming::increaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    W.insertVertex(wu);
    auto u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
    if (inCover(u)) {
        removeFromCover(u);
        auto &nu = W.nodes[u].neighbors;
        for (auto &e : nu) {
            if (!inCover(e)) {
                update(order(u, e));
                if (inCover(u)) return;
            }
        }
    }
}

DynamicPricingStreaming::DynamicPricingStreaming() {
    W.edges.clear();
}

bool DynamicPricingStreaming::inCover(NodeId u) {
    return C.find(u) != C.end();
}

void DynamicPricingStreaming::removeFromCover(NodeId u) {
    if (inCover(u)) C.erase(u);
}



void run_dynamic_experiment_streaming(const TemporalGraphStream& tgs, Params const &params) {

    auto delta = params.delta;
    auto stepsize = params.stepsize;

    uint pos = params.start_pos;

    Time start = tgs.edges[pos].t;
    Time end = tgs.edges.back().t;

    uint cur_start = start,
         cur_end = start + delta - 1;

    DynamicPricingStreaming dynamic_MWVC;

//    TemporalEdges edges;
    list<TemporalEdge> edges;

    Graph graph(tgs.num_nodes);

#ifdef PRINT_TIMES
    Timer timer;
    vector<double> times;
    vector<uint> num_edges;
    vector<uint> num_nodes;
    vector<uint> C_size;

    double time = 0;
    double aggr_time = 0;
#endif
    uint po = 0;

    while (tgs.edges[pos].t <= end - delta + 1) {


        TemporalEdges new_edges, removed_edges;

        if (!edges.empty()) {
            int a = tgs.edges[pos].t - cur_end;
            int b = edges.front().t - cur_start;
            int c = min(a, b);
            if (c > 0) {
                cur_start += c;
                cur_end += c;
            }
        }

        auto it = edges.begin();
        while (it != edges.end()) {
            if ((*it).t < cur_start) {
                removed_edges.push_back(*it);
                it = edges.erase(it);
            } else {
                break;
            }
        }

        while (tgs.edges[pos].t <= cur_end) {
            if (tgs.edges[pos].u_id != tgs.edges[pos].v_id)
                new_edges.push_back(tgs.edges[pos]);
            pos++;
            if (pos >= tgs.edges.size()) break;
        }

        cur_start += stepsize;
        cur_end = min((ulong)stepsize + cur_end, end);



        if (removed_edges.empty() && new_edges.empty()) continue;

        edges.insert(edges.end(), new_edges.begin(), new_edges.end());


//        if (++po % 100==0)
//            cout << pos << " " << cur_start << " " << cur_end << " " << tgs.edges[pos].t << " " << end << " " <<
//                 removed_edges.size() << " " << new_edges.size() << endl;

#ifdef PRINT_TIMES
        timer.start();
#endif

        auto sigma = update_aggregation_and_wedge_graph(new_edges, removed_edges, graph);

#ifdef PRINT_TIMES
        aggr_time += timer.stop();
#endif
#ifdef PRINT_TIMES
        if (po % 10000 == 0) {
            cout << "delta:     \t" << delta << endl;
            cout << "e.tl - e.t1\t" << edges.back().t - edges.front().t << endl;
            cout << "Time aggr:\t" << aggr_time << endl;
            aggr_time = 0;
            cout << "edges size:\t" << edges.size() << endl;
            cout << "new edges:\t" << new_edges.size() << endl;
            cout << "rem. edges:\t" << removed_edges.size() << endl;
            cout << "Sigma len:\t" << sigma.size() << endl;
            cout << "#A.nodes:\t" << graph.nodes.size() << endl;
            cout << "#A.edges:\t" << graph.num_edges << endl;
            cout << "#W.nodes:\t" << dynamic_MWVC.W.nodes.size() << endl;
            cout << "#W.edges:\t" << dynamic_MWVC.W.num_edges << endl;
            cout << "#C before: " << dynamic_MWVC.C.size() << endl;
        }
        timer.start();
#endif

        dynamic_MWVC.run_update_sequence(sigma);

//        cout << graph.nodes.size() << " " << graph.num_edges << endl;

#ifdef PRINT_TIMES
        auto t = timer.stop();
        time += t;
//        times.push_back(t);
//        num_edges.push_back(dynamic_MWVC.W.num_edges);
//        num_nodes.push_back(dynamic_MWVC.W.nodes.size());
//        C_size.push_back(dynamic_MWVC.C.size());

        if (po % 10000 == 0) {
            cout << "Iter:\t" << po << endl;
            cout << "Time:\t" << t << endl;
            cout << "Time sum:\t" << time << endl;
            cout << "Pos: \t" << pos << " of " << tgs.edges.size() << endl;
            cout << "------------------------------------------" << endl;
        }
#endif
        po++;
//        if (po == params.windows_to_process) break;

    }
#ifdef PRINT_TIMES
//    write_results_to_file(params, times, num_edges, num_nodes, C_size);
    SGLog::log() << "running time complete: " << time << endl;
    cout << fixed;
    cout << time << endl;

#endif
#ifdef PRINT_DATA_TO_FILES
    dynamic_MWVC.save_data(params.dataset_path);
#endif
}

#ifdef PRINT_DATA_TO_FILES
void DynamicPricingStreaming::update_counts() {
    if (C_weights.size() > MAX_ITERATIONS_TO_PRINT) return;
//    if (W.num_edges == 0) return;
    C_sizes.push_back(C.size());
    C_weights.push_back(0);
    for (auto &c : C) {
        C_weights.back() += (uint)W.nodes[c].weight;
    }
    num_time_windows++;
    C_counts.emplace_back();
    for (auto &c : C) {
        C_counts.back().push_back(W.nodes[c].e);
    }
    W_edges.push_back(W.num_edges);
}

void DynamicPricingStreaming::save_data(const string &outfile) {
//    auto counts_file = outfile + ".ccounts";
//    auto sizes_file = outfile + ".csizes";
    auto wedges_file = outfile + ".wedges";
    auto weights_file = outfile + ".cweights";
//    vector<string> normalized;
//    normalized.push_back(to_string(C_counts.size()));
//    for (auto &Cv : C_counts) {
//        for (auto &p : Cv) {
//            string s = to_string(p.first) + " " + to_string(p.second);
//            normalized.push_back(s);
//        }
//        normalized.emplace_back("\n");
//    }
//    HF::writeVectorToFile(counts_file, normalized);
//    HF::writeVectorToFile(sizes_file, C_sizes);
    HF::writeVectorToFile(wedges_file, W_edges);
    HF::writeVectorToFile(weights_file, C_weights);

}
#endif

void write_results_to_file(const Params &params, std::vector<double> times,
                           std::vector<uint> num_edges, std::vector<uint> num_nodes,
                           std::vector<uint> C_sizes) {
    auto times_file = params.dataset_path + "_"
                      + to_string(params.mode) + "_" + to_string(params.delta) + ".times";
    auto num_edges_file = params.dataset_path + "_"
                          + to_string(params.mode) + "_" + to_string(params.delta) + ".edges";
    auto num_nodes_file = params.dataset_path + "_"
                          + to_string(params.mode) + "_" + to_string(params.delta) + ".nodes";
    auto C_sizes_file = params.dataset_path + "_"
                        + to_string(params.mode) + "_" + to_string(params.delta) + ".csizes";

    HF::writeVectorToFile(times_file, times);
    HF::writeVectorToFile(num_edges_file, num_edges);
    HF::writeVectorToFile(num_nodes_file, num_nodes);
    HF::writeVectorToFile(C_sizes_file, C_sizes);
}