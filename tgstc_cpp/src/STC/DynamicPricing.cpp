#include "DynamicPricing.h"
#include "../Utils/HelperFunctions.h"

#include <utility>
#include <cassert>

using namespace std;

bool verify_cover(const WedgeGraph& wedgeGraph, const Cover& C) {
    for (auto &n : wedgeGraph.nodes) {
        for (auto &e : n.adjlist) {
            if (C.find(e.u) == C.end() && C.find(e.v) == C.end()) {
                cout << e.u << " " << e.v << endl;
                return false;
            }
        }
    }
    return true;
}

bool verify_cover(const WedgeGraphDynamic& wedgeGraph, const Cover& C) {
    for (auto &n : wedgeGraph.nodes) {
        for (auto &v : n.neighbors) {
            if (C.find(n.id) == C.end() && C.find(v) == C.end()) {
                cout << n.id << " " << v << endl;
                return false;
            }
        }
    }
    return true;
}

bool verify_cover(const WedgeGraph& wedgeGraph, const vector<bool>& C) {
    for (auto &n : wedgeGraph.nodes) {
        for (auto &e : n.adjlist) {
            if (!C[e.u] && !C[e.v]) {
                cout << e.u << " " << e.v << endl;
                return false;
            }
        }
    }
    return true;
}


void get_wedge_triangle_updates_add(Graph &A, NodeId u, NodeId v, Sigma& sigma) {
    for (auto &nu : A.nodes[u].neighbors) {
        if (A.nodes[v].neighbors.find(nu) != A.nodes[v].neighbors.end()) {
            Update update{DE,  order(u, nu), order(v, nu)};
            sigma.push_back(update);
        } else if (nu != v) {
            Update update{IE,  order(u, v), order(u, nu)};
            sigma.push_back(update);
        }
    }

    for (auto &nv : A.nodes[v].neighbors) {
        if (A.nodes[u].neighbors.find(nv) == A.nodes[u].neighbors.end() && u != nv) {
            Update update{IE,  order(u, v), order(v, nv)};
            sigma.push_back(update);
        }
    }
}

void get_wedge_triangle_updates_remove(Graph &A, NodeId u, NodeId v, Sigma& sigma) {
    for (auto &nu : A.nodes[u].neighbors) {
        if (A.nodes[v].neighbors.find(nu) != A.nodes[v].neighbors.end()) {
            assert(u != nu && v != nu && u!=v);
            Update update{IE,  order(u, nu), order(v, nu)};
            sigma.push_back(update);
        } else if (nu != v) {
            Update update{DE,  order(u, v), order(u, nu)};
            sigma.push_back(update);
        }
    }

    for (auto &nv : A.nodes[v].neighbors) {
        if (A.nodes[u].neighbors.find(nv) == A.nodes[u].neighbors.end() && u != nv) {
            Update update{DE, order(u, v), order(v, nv)};
            sigma.push_back(update);
        }
    }
}


Sigma update_aggregation_and_wedge_graph(const TemporalEdges& new_edges, const TemporalEdges& removed_edges, Graph& A) {
   Sigma sigma;
   map<pair<NodeId, NodeId>, double> changes_in_A;
   for (auto &e : new_edges) {
       changes_in_A[order(e.u_id, e.v_id)]++;
   }
    for (auto &e : removed_edges) {
        changes_in_A[order(e.u_id, e.v_id)]--;
    }

    for (auto &k : changes_in_A) {
        auto d = k.second;
        if (d == 0) continue;
        auto u = k.first.first;
        auto v = k.first.second;
//        assert(u != v);
        if (d > 0) {
            if (!A.has_edge(u, v)) {
                A.insertEdge(u, v, d);
                Update update{CW, {u, v}, {0, 0}, d};
                sigma.push_back(update);
                get_wedge_triangle_updates_add(A, u, v, sigma);
            } else {
                A.addWeight(u, v, d);
                Update update{CW, {u, v}, {0, 0}, d};
                sigma.push_back(update);
            }
        }
        if (d < 0) {
            A.addWeight(u, v, d);
            Update update{CW, {u, v}, {0, 0}, d};
            sigma.push_back(update);
        }
        if (A.getWeight(u, v) == 0) {
            A.removeEdge(u, v);
            get_wedge_triangle_updates_remove(A, u, v, sigma);
            Update update{RN, {u, v}, {0,0}, 0};
            sigma.push_back(update);
        }
    }

   return sigma;
}

