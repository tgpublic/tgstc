#ifndef CPPSOURCE_PRICINGAPPROX_H
#define CPPSOURCE_PRICINGAPPROX_H

#include "../Transformation/TransformGraph.h"

std::unordered_set<NodeId> pricing_approx(WedgeGraph const &w);

#endif //CPPSOURCE_PRICINGAPPROX_H
