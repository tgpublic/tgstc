#ifndef CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_H
#define CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_H
#include "../Temporalgraph/TemporalGraphs.h"
#include "../Transformation/TransformGraph.h"
#include "Utils/Params.h"
#include "DynamicPricing.h"
#include <unordered_set>
#include <set>
#include <map>


class DynamicPricingSlowStream {

public:

    DynamicPricingSlowStream();

    void run_update_sequence(Sigma const &sigma);

    void save_data(std::string const &outfile);

    Cover C;
    WedgeGraphDynamic W;

private:

    void update();

    void insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void decreaseWeight(std::pair<NodeId, NodeId> u, double d);

    void increaseWeight(std::pair<NodeId, NodeId> u, double d);

    void update_counts();



    std::vector<uint> C_sizes;
//    std::vector<uint> C_counts;
    std::vector<std::vector<std::pair<NodeId, NodeId>>> C_counts;
    std::vector<uint> C_weights;
    std::vector<uint> W_edges;
    uint num_time_windows = 0;

};

void run_dynamic_experiment_slow_stream(const TemporalGraphStream& tgs, Params const &params);


#endif //CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_H
