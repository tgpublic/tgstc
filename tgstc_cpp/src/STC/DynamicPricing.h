#ifndef CPPSOURCE_DYNAMICPRICING_H
#define CPPSOURCE_DYNAMICPRICING_H

// #define PRINT_DATA_TO_FILES
#define MAX_ITERATIONS_TO_PRINT 1000000
#define PRINT_TIMES


#include "../Temporalgraph/TemporalGraphs.h"
#include "../Transformation/TransformGraph.h"
#include "../Utils/Params.h"
#include <unordered_set>
#include <set>
#include <map>

using Cover = std::unordered_set<NodeId>;
using EdgesV = std::vector<std::pair<NodeId, NodeId>>;
using Prices = std::map<std::pair<NodeId, NodeId>, double>;

enum UpdateType {
    IE, DE, CW, RN
};
struct Update {
    UpdateType updateType;
    std::pair<NodeId, NodeId> wu, wv;
    double d;
};

using Sigma = std::vector<Update>;

class DynamicPricing {

public:

    DynamicPricing(Graph A1, WedgeGraph W1);

    void run_update_sequence(Sigma const &sigma);

    void save_data(std::string const &outfile);

private:

    void update(const EdgesV& F);

    void insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void decreaseWeight(std::pair<NodeId, NodeId> u, double d);

    void increaseWeight(std::pair<NodeId, NodeId> u, double d);

    bool isTight(NodeId u);

    bool inCover(NodeId u);

    void removeFromCover(NodeId u);

    void update_counts();

    Cover C;
//    std::vector<bool> C;
    Graph A;
    WedgeGraph W;
    Prices prices;
    std::vector<double> price_sum;

    std::vector<uint> C_sizes;
    std::vector<uint> C_counts;
    std::vector<uint> C_weights;
    std::vector<uint> W_edges;
    uint num_time_windows = 0;
};

Sigma update_aggregation_and_wedge_graph(const TemporalEdges& new_edges, const TemporalEdges& removed_edges, Graph& A);

bool verify_cover(const WedgeGraphDynamic& wedgeGraph, const Cover& C);

#endif //CPPSOURCE_DYNAMICPRICING_H
