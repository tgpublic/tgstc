#ifndef CPPSOURCE_DYNAMICSTREAMSTATS_H
#define CPPSOURCE_DYNAMICSTREAMSTATS_H

#include "DynamicPricing.h"

class DynamicStreamStats {

public:

    void run_update_sequence(Sigma const &sigma);

    WedgeGraphDynamic W;

private:

    void insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void decreaseWeight(std::pair<NodeId, NodeId> u, double d);

    void increaseWeight(std::pair<NodeId, NodeId> u, double d);

};

void run_dynamic_experiment_stats(const TemporalGraphStream& tgs, Params const &params);


#endif //CPPSOURCE_DYNAMICSTREAMSTATS_H
