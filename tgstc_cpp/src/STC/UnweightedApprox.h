#ifndef CPPSOURCE_UNWEIGHTEDAPPROX_H
#define CPPSOURCE_UNWEIGHTEDAPPROX_H

#include "../Transformation/TransformGraph.h"

std::unordered_set<NodeId> unweighted_approx_matching(WedgeGraph const &w, Graph const &g);

std::unordered_set<NodeId> unweighted_approx_highdeg(WedgeGraph const &w, Graph const &g);

std::map<std::pair<NodeId,NodeId>, uint> get_common_neighbors(Graph const& g);

#endif //CPPSOURCE_UNWEIGHTEDAPPROX_H
