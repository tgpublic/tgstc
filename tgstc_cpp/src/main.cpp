#include <iostream>
#include <vector>
#include "Utils/Params.h"
#include "Temporalgraph/TemporalGraphStreamProvider.h"
#include "Utils/SGLog.h"
#include "Utils/HelperFunctions.h"
#include "Transformation/TransformGraph.h"
#include "STC/PricingApprox.h"
#include "STC/UnweightedApprox.h"
#include "STC/DynamicPricingStreaming.h"
#include "STC/DynamicPricingSlowStream.h"
#include "STC/DynamicStreamStats.h"
#include "STCplus/DynamicPricingStreaming_STCplus.h"
#include "STCplus/DynamicPricingSlowStream_STCplus.h"
#include "STCplus/PricingApprox_STCplus.h"

using namespace std;


void do_tranformation(TemporalGraphStream const &tgs, Params const &params) {
    SGLog::log() << "computing aggregated graph" << endl;
    auto g = temporalGraphToGraph(tgs);
    g.save(params.dataset_path + ".ag");
    SGLog::log() << "#nodes: " << g.nodes.size() << endl;
    SGLog::log() << "#edges: " << g.num_edges << endl;

    SGLog::log() << "computing wedge graph" << endl;
    auto w = graphToWedgeGraph(g);
    w.save(params.dataset_path + ".ag.wg");
    SGLog::log() << "#nodes: " << w.nodes.size() << endl;
    SGLog::log() << "#edges: " << w.num_edges << endl;
}


void print_mean_weights(unordered_set<NodeId> const &C, WedgeGraph const &w) {
    vector<double> strong_weights;
    vector<double> weak_weights;
    double sum_weak = 0;
    double sum_strong = 0;
    for (auto &n : w.nodes) {
        if (C.find(n.id) == C.end()) {
            strong_weights.push_back(n.weight);
            sum_strong += n.weight;
        }
        else {
            weak_weights.push_back(n.weight);
            sum_weak += n.weight;
        }
    }
    double strong_mean, weak_mean, stdev;
    HF::get_mean_std(strong_weights, strong_mean, stdev);
    HF::get_mean_std(weak_weights, weak_mean, stdev);
    cout << weak_mean << " & " << strong_mean << endl;
}


void compute_vc(Params const &params, string const &ext) {

    Graph g(0);
    g.load(params.dataset_path);
    SGLog::log() << "#nodes: " << g.nodes.size() << endl;
    SGLog::log() << "#edges: " << g.num_edges << endl;

    auto wg_dataset_path = params.dataset_path + ext;

    SGLog::log() << "loading wedge graph" << endl;
    WedgeGraph w;
    w.load(wg_dataset_path);
    SGLog::log() << "#nodes: " << w.nodes.size() << endl;
    SGLog::log() << "#edges: " << w.num_edges << endl;

    auto C = pricing_approx(w);
    cout << "#cover (approx): " << C.size() << endl;
    cout << "% of strong edges: " << 100.0*(double)(w.nodes.size()-C.size())/(double)w.nodes.size() << endl;
    print_mean_weights(C, w);
    vector<string> output;
    for (auto v : C) {
        string o = to_string(v) + " " + to_string(w.nodes[v].e.first) + " " + to_string(w.nodes[v].e.second);
        output.push_back(o);
    }
    HF::writeVectorToFile(wg_dataset_path + ".vcp", output);


    auto C2 = unweighted_approx_matching(w, g);
    cout << "#cover (matching): " << C2.size() << endl;
    cout << "% of strong edges: " << 100.0*(double)(w.nodes.size()-C2.size())/(double)w.nodes.size() << endl;
    print_mean_weights(C2, w);
    vector<string> output2;
    for (auto v : C2) {
        string o = to_string(v) + " " + to_string(w.nodes[v].e.first) + " " + to_string(w.nodes[v].e.second);
        output2.push_back(o);
    }
    HF::writeVectorToFile(wg_dataset_path + ".vcm", output2);


    auto C3 = unweighted_approx_highdeg(w, g);
    cout << "#cover (highdeg): " << C3.size() << endl;
    cout << "% of strong edges: " << 100.0*(double)(w.nodes.size()-C3.size())/(double)w.nodes.size() << endl;
    print_mean_weights(C3, w);
    vector<string> output3;
    for (auto v : C3) {
        string o = to_string(v) + " " + to_string(w.nodes[v].e.first) + " " + to_string(w.nodes[v].e.second);
        output3.push_back(o);
    }
    HF::writeVectorToFile(wg_dataset_path + ".vch", output3);
}

void compute_vc_hypergraph(Params const &params, string const &ext) {
    Graph g(0);
    g.load(params.dataset_path);
    SGLog::log() << "#nodes: " << g.nodes.size() << endl;
    SGLog::log() << "#edges: " << g.num_edges << endl;

    auto wg_dataset_path = params.dataset_path + ext;
    SGLog::log() << "loading wedge graph" << endl;
    WedgeGraph w;
    w.load(wg_dataset_path);
    SGLog::log() << "#nodes: " << w.nodes.size() << endl;
    SGLog::log() << "#edges: " << w.num_edges << endl;
    WedgeHyperGraph whg;
    whg.load(wg_dataset_path, params.alpha);
    SGLog::log() << "loading wedge hypergraph" << endl;
    SGLog::log() << "#nodes: " << whg.nodes.size() << endl;
    SGLog::log() << "#edges: " << whg.num_edges << endl;

//    auto pricing = pricing_approx_STCplus2(whg, g);
    auto pricing = pricing_approx_STCplus(whg);
    cout << "#cover (STCplus pricing): " << pricing.size() << endl;
    cout << "% of strong edges: " << 100.0*(double)(w.nodes.size()-pricing.size())/(double)w.nodes.size() << endl;
    print_mean_weights(pricing, w);

    vector<string> output_pricing;
    for (auto v : pricing) {
        string o = to_string(v) + " " + to_string(w.nodes[v].e.first) + " " + to_string(w.nodes[v].e.second);
        output_pricing.push_back(o);
    }
    HF::writeVectorToFile(wg_dataset_path + ".vspx", output_pricing);

}


TemporalGraphStream get_tgs(Params const &params) {
    TemporalGraphStreamProvider p;
    p.loadTemporalGraph(params.dataset_path, params.lines);
    auto tgs = p.getTGS();
    SGLog::log() << "#tgs nodes: " << p.getTGS().num_nodes << endl;
    SGLog::log() << "#tgs edges: " << p.getTGS().edges.size() << endl;
    return tgs;
}


void dynamic_experiment_dynamic(TemporalGraphStream &tgs, Params const &params) {
#ifdef PRINT_DATA_TO_FILES
    SGLog::log() << "PRINT_DATA_TO_FILES" << endl;
#else
    SGLog::log() << "No data output" << endl;
#endif

    Timer timer;
    SGLog::log() << "running dynamic experiment" << endl;
    timer.start();
    run_dynamic_experiment_streaming(tgs, params);
    timer.stopAndPrintTime();

}

void dynamic_experiment_dynamic_STCplus(TemporalGraphStream &tgs, Params const &params) {
#ifdef PRINT_DATA_TO_FILES
    SGLog::log() << "PRINT_DATA_TO_FILES" << endl;
#else
    SGLog::log() << "No data output" << endl;
#endif

    Timer timer;
    SGLog::log() << "running dynamic experiment (STCplus)" << endl;
    timer.start();
    run_dynamic_experiment_streaming_STCplus(tgs, params);
    timer.stopAndPrintTime();

}


void dynamic_experiment_dynamic_slow(TemporalGraphStream &tgs, Params const &params) {
#ifdef PRINT_DATA_TO_FILES
    SGLog::log() << "PRINT_DATA_TO_FILES" << endl;
#else
    SGLog::log() << "No data output" << endl;
#endif

    Timer timer;
    SGLog::log() << "running dynamic experiment" << endl;
    timer.start();
    run_dynamic_experiment_slow_stream(tgs, params);
    timer.stopAndPrintTime();

}

void dynamic_experiment_dynamic_slow_STCplus(TemporalGraphStream &tgs, Params const &params) {
#ifdef PRINT_DATA_TO_FILES
    SGLog::log() << "PRINT_DATA_TO_FILES" << endl;
#else
    SGLog::log() << "No data output" << endl;
#endif

    Timer timer;
    SGLog::log() << "running dynamic experiment" << endl;
    timer.start();
    run_dynamic_experiment_slow_stream_STCplus(tgs, params);
    timer.stopAndPrintTime();

}


void dynamic_experiment_stats(TemporalGraphStream &tgs, Params const &params) {
    Timer timer;
    SGLog::log() << "running dynamic experiment stats" << endl;
    timer.start();
    run_dynamic_experiment_stats(tgs, params);
    timer.stopAndPrintTime();
}

int main(int argc, char *argv[]) {
    srand((unsigned) time(nullptr));

    vector <string> args;
    for (size_t i = 1; i < argc; ++i)
        args.emplace_back(argv[i]);

    Params params;
    if (!params.parseArgs(args))
        show_help();

    SGLog::log() << "Loading " << params.dataset_path << endl;

    if (params.mode == 1) {
        auto tgs = get_tgs(params);
        do_tranformation(tgs, params);
    } else if (params.mode == 2) {
        compute_vc(params, ".wg");
    } else if (params.mode == 20) {
        compute_vc_hypergraph(params, ".wg");
    } else if (params.mode == 3) {
        auto tgs = get_tgs(params);
        dynamic_experiment_dynamic(tgs, params);
    } else if (params.mode == 4) {
        auto tgs = get_tgs(params);
        dynamic_experiment_dynamic_slow(tgs, params);
    } else if (params.mode == 5) {
        auto tgs = get_tgs(params);
        dynamic_experiment_dynamic_STCplus(tgs, params);
    } else if (params.mode == 6) {
        auto tgs = get_tgs(params);
        dynamic_experiment_dynamic_slow_STCplus(tgs, params);
    }
}