#include "DynamicPricingStreaming_STCplus.h"
#include "../Utils/HelperFunctions.h"
#include "Utils/SGLog.h"
#include "Transformation/WedgeHyperGraph.h"

#include <utility>
#include <cassert>
#include <functional>

using namespace std;

pair<NodeId, NodeId> get_vedge_nodeId(pair<NodeId, NodeId> u, pair<NodeId, NodeId> v) {
    pair<NodeId, NodeId> v_edge;
    if (u.first == v.first) {
        v_edge = order(u.second, v.second);
    } else if (u.first == v.second) {
        v_edge = order(u.second, v.first);
    } else if (u.second == v.first) {
        v_edge = order(u.first, v.second);
    } else if (u.second == v.second) {
        v_edge = order(u.first, v.first);
    }
    return v_edge;
}

void DynamicPricingStreaming_STCplus::run_update_sequence(Sigma const &sigma) {
    if (sigma.empty()) return;
    for (size_t i = 0; i < sigma.size(); i++) {
        auto s = sigma[i];
    // for (auto &s : sigma) {
        auto u = order(s.wu.first, s.wu.second);
        auto v = order(s.wv.first, s.wv.second);



        switch (s.updateType) {
            case IE: {
                auto v_edge = get_vedge_nodeId(u, v);
                insertEdge(u, v, v_edge);
                break;
            }
            case DE: {
                deleteEdge(u, v);


                break;
            }
            case CW: {
                assert(s.d != 0);
                if (s.d > 0) increaseWeight(u, s.d);
                else decreaseWeight(u, s.d);
                break;
            }
            case RN: {
                auto node = W.edgeNodeMap[order(u.first, u.second)];
                // if (inCover(node)) C.erase(node);
                W.nodes[node].neighbors.clear();
                // assert(W.nodes[node].neighbors.empty());
                W.nodes[node].weight = 0;
                // W.deleteVertex(u);
                removeFromCover(node);
                break;
            }
        }
    }
//    cout << C.size() << endl;
#ifdef PRINT_DATA_TO_FILES
    update_counts();
#endif
}

void DynamicPricingStreaming_STCplus::update(const WHyperEdge &e) {
    auto const u = e.u, v = e.v, w = e.w ;

    if (inCover(u) || inCover(v) || inCover(w)) return;

    const auto &node_u = W.nodes[u];
    const auto &node_v = W.nodes[v];
    const auto &node_w = W.nodes[w];

    // Calculate the differences
    const auto diffu = node_u.weight - node_u.price_sum;
    const auto diffv = node_v.weight - node_v.price_sum;
    const auto diffw = node_w.weight - node_w.price_sum;

    // Calculate the minimum difference
    const auto diff = std::min({diffu, diffv, diffw});

    prices[{u, v, w}] += diff;

    W.nodes[u].price_sum += diff;
    W.nodes[v].price_sum += diff;
    W.nodes[w].price_sum += diff;

    if (W.nodes[u].isTight()) C.insert(u);
    if (W.nodes[v].isTight()) C.insert(v);
    if (W.nodes[w].isTight()) C.insert(w);
}

void DynamicPricingStreaming_STCplus::update(const vector<WHyperEdge> &F) {
    // set<WHyperEdge> Fset {F.begin(), F.end()};
    for (auto &e : F) {
        update(e);
    }
// todo   assert(verify_cover(W, C));
}

void DynamicPricingStreaming_STCplus::insertEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv, pair<NodeId, NodeId> v_edge) {
    W.addEdge(wu, wv, v_edge);
    auto const u = W.edgeNodeMap[wu];
    auto const v = W.edgeNodeMap[wv];
    auto const w = W.edgeNodeMap[v_edge];
    auto const f = order(u, v, w);
    // todo
    // assert(prices.find(f) == prices.end());
    // if (prices.find(f) != prices.end()) {
        // cout << prices[f] << endl;
    // }
    prices[f] = 0;
    update(f);
}

void DynamicPricingStreaming_STCplus::deleteEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv) {
    auto v_edge = get_vedge_nodeId(wu, wv);
    NodeId const u = W.edgeNodeMap[wu], v = W.edgeNodeMap[wv], w = W.edgeNodeMap[v_edge];
    auto const p = order(u, v, w);
    W.removeEdge(p.u, p.v, p.w);
    auto const price = prices[p];
    prices.erase(p);
    W.nodes[u].price_sum -= price;
    W.nodes[v].price_sum -= price;
    W.nodes[w].price_sum -= price;
    if (!W.nodes[u].isTight())
        removeFromCover(u);
    if (!W.nodes[v].isTight())
        removeFromCover(v);
    if (!W.nodes[w].isTight())
        removeFromCover(w);

    // if (--W.nodeDegree[u] == 0) W.deleteVertex(wu);
    // if (--W.nodeDegree[v] == 0) W.deleteVertex(wv);
    // if (--W.nodeDegree[w] == 0) W.deleteVertex(v_edge);

    vector<WHyperEdge> F;
    for (const auto &[fst, snd] : W.nodes[u].neighbors) {
        if (!inCover(fst) && !inCover(snd)) {
            F.push_back(order(u, fst, snd));
        }
    }
    for (const auto &[fst, snd] : W.nodes[v].neighbors) {
        if (!inCover(fst) && !inCover(snd)) {
            F.push_back(order(v, fst, snd));
        }
    }
    update(F);
}

void DynamicPricingStreaming_STCplus::decreaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    auto const u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
    // cout << u << " " << W.nodes[u].weight << endl;
    // assert(W.nodes[u].weight > 0);
    if (inCover(u)) removeFromCover(u);
    auto const &Fp = W.nodes[u].neighbors;
    vector<WHyperEdge> F;

    for (const auto &n : Fp) {
        auto const p = prices[order(u, n.first, n.second)];
        prices[order(u, n.first, n.second)] = 0;
        W.nodes[u].price_sum -= p;
        W.nodes[n.first].price_sum -= p;
        W.nodes[n.second].price_sum -= p;
        if (!W.nodes[n.first].isTight()) {
            F.push_back(order(u, n.first, n.second));
            if (inCover(n.first)) removeFromCover(n.first);
            auto &nv = W.nodes[n.first].neighbors;
            for (auto const &f : nv) {
                if (!W.nodes[f.first].isTight() && !W.nodes[f.second].isTight()) {
                    F.push_back(order(n.first, f.first, f.second));
                }
            }
        }
        if (!W.nodes[n.second].isTight()) {
            F.push_back(order(u, n.first, n.second));
            if (inCover(n.second)) removeFromCover(n.second);
            auto &nv = W.nodes[n.second].neighbors;
            for (auto const &f : nv) {
                if (!W.nodes[f.first].isTight() && !W.nodes[f.second].isTight()) {
                    F.push_back(order(n.second, f.first, f.second));
                }
            }
        }
    }
    update(F);
}


void DynamicPricingStreaming_STCplus::increaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    W.insertVertex(wu, false);
    auto const u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
    if (inCover(u)) {
        removeFromCover(u);
        auto const &nu = W.nodes[u].neighbors;
        for (auto const &e : nu) {
            if (!inCover(e.first) && !inCover(e.second)) {
                update(order(u, e.first, e.second));
                if (inCover(u)) return;
            }
        }
    }
}

DynamicPricingStreaming_STCplus::DynamicPricingStreaming_STCplus() {
    W.edges.clear();
}

bool DynamicPricingStreaming_STCplus::inCover(NodeId u) {
    return C.find(u) != C.end();
}

void DynamicPricingStreaming_STCplus::removeFromCover(NodeId u) {
    if (inCover(u)) C.erase(u);
}



void run_dynamic_experiment_streaming_STCplus(const TemporalGraphStream& tgs, Params const &params) {

    auto const delta = params.delta;
    auto const stepsize = params.stepsize;

    uint pos = params.start_pos;

    Time const start = tgs.edges[pos].t;
    Time const end = tgs.edges.back().t;

    uint cur_start = start;
    uint cur_end = start + delta - 1;

    DynamicPricingStreaming_STCplus dynamic_MWVC;

//    TemporalEdges edges;
    list<TemporalEdge> edges;

    Graph graph(tgs.num_nodes);

#ifdef PRINT_TIMES
    Timer timer;
    vector<double> times;
    vector<uint> num_edges;
    vector<uint> num_nodes;
    vector<uint> C_size;

    double time = 0;
    double aggr_time = 0;
#endif
    uint po = 0;

    while (tgs.edges[pos].t <= end - delta + 1) {


        TemporalEdges new_edges, removed_edges;

        if (!edges.empty()) {
            auto a = tgs.edges[pos].t - cur_end;
            auto b = edges.front().t - cur_start;
            auto c = min(a, b);
            if (c > 0) {
                cur_start += c;
                cur_end += c;
            }
        }

        auto it = edges.begin();
        while (it != edges.end()) {
            if ((*it).t < cur_start) {
                removed_edges.push_back(*it);
                it = edges.erase(it);
            } else {
                break;
            }
        }

        while (tgs.edges[pos].t <= cur_end) {
            if (tgs.edges[pos].u_id != tgs.edges[pos].v_id)
                new_edges.push_back(tgs.edges[pos]);
            pos++;
            if (pos >= tgs.edges.size()) break;
        }

        cur_start += stepsize;
        cur_end = min((ulong)stepsize + cur_end, end);


        if (removed_edges.empty() && new_edges.empty()) {
//            cout << pos << " empty" << endl;
            continue;
        }

        edges.insert(edges.end(), new_edges.begin(), new_edges.end());


//        if (++po % 100==0)
//            cout << pos << " " << cur_start << " " << cur_end << " " << tgs.edges[pos].t << " " << end << " " <<
//                 removed_edges.size() << " " << new_edges.size() << endl;

#ifdef PRINT_TIMES
        timer.start();
#endif
        auto sigma = update_aggregation_and_wedge_graph(new_edges, removed_edges, graph);
#ifdef PRINT_TIMES
        aggr_time += timer.stop();
#endif
#ifdef PRINT_TIMES
        if (po % 10000 == 0) {
            cout << "delta:     \t" << delta << endl;
            cout << "e.tl - e.t1\t" << edges.back().t - edges.front().t << endl;
            cout << "Time aggr:\t" << aggr_time << endl;
            aggr_time = 0;
            cout << "edges size:\t" << edges.size() << endl;
            cout << "new edges:\t" << new_edges.size() << endl;
            cout << "rem. edges:\t" << removed_edges.size() << endl;
            cout << "Sigma len:\t" << sigma.size() << endl;
            cout << "#A.nodes:\t" << graph.nodes.size() << endl;
            cout << "#A.edges:\t" << graph.num_edges << endl;
            // cout << "#W.nodes:\t" << dynamic_MWVC.W.num_nodes << endl;
            cout << "#W.edges:\t" << dynamic_MWVC.W.num_edges << endl;
            cout << "#C before: " << dynamic_MWVC.C.size() << endl;
        }
        timer.start();
#endif

        dynamic_MWVC.run_update_sequence(sigma);

//        cout << graph.nodes.size() << " " << graph.num_edges << endl;

#ifdef PRINT_TIMES
        auto const t = timer.stop();
        time += t;
//        times.push_back(t);
//        num_edges.push_back(dynamic_MWVC.W.num_edges);
//        num_nodes.push_back(dynamic_MWVC.W.nodes.size());
//        C_size.push_back(dynamic_MWVC.C.size());

        if (po % 10000 == 0) {
            cout << "Iter:\t" << po << endl;
            cout << "Time:\t" << t << endl;
            cout << "Time sum:\t" << time << endl;
            cout << "Pos: \t" << pos << " of " << tgs.edges.size() << endl;
            cout << "------------------------------------------" << endl;
        }
#endif
        po++;
//        if (po == params.windows_to_process) break;

    }
#ifdef PRINT_TIMES
//    write_results_to_file(params, times, num_edges, num_nodes, C_size);
    SGLog::log() << "running time complete: " << time << endl;
    cout << fixed;
    cout << time << endl;

#endif
#ifdef PRINT_DATA_TO_FILES
    dynamic_MWVC.save_data(params.dataset_path);
#endif
}

#ifdef PRINT_DATA_TO_FILES
void DynamicPricingStreaming_STCplus::update_counts() {
    uint Csize = 0;
    C_weights.push_back(0);
    for (auto &n : W.nodes) {
        if (inCover(n.id)) {
            Csize++;
            C_weights.back() += n.weight;
        }
    }
    C_sizes.push_back(Csize);
    num_time_windows++;
    C_counts.emplace_back();
    for (auto &c : C) {
        C_counts.back().push_back(W.nodes[c].e);
    }
    W_edges.push_back(W.num_edges);
//    Cs.push_back(C);
}

void DynamicPricingStreaming_STCplus::save_data(const string &outfile) {
    auto counts_file = outfile + "_stcplus.ccounts";
    auto sizes_file = outfile + "_stcplus.csizes";
    auto wedges_file = outfile + "_stcplus.wedges";
    auto weights_file = outfile + "_stcplus.cweights";
    vector<string> normalized;
    normalized.push_back(to_string(C_counts.size()));
    for (auto &Cv : C_counts) {
        for (auto &p : Cv) {
            string s = to_string(p.first) + " " + to_string(p.second);
            normalized.push_back(s);
        }
        normalized.emplace_back("\n");
    }
    HF::writeVectorToFile(counts_file, normalized);
    HF::writeVectorToFile(sizes_file, C_sizes);
    HF::writeVectorToFile(wedges_file, W_edges);
    HF::writeVectorToFile(weights_file, C_weights);

}
#endif

