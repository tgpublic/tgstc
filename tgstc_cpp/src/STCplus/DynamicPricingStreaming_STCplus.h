#ifndef CPPSOURCE_DYNAMICPRICINGSTREAMING_STCPLUS_H
#define CPPSOURCE_DYNAMICPRICINGSTREAMING_STCPLUS_H

#include "../Temporalgraph/TemporalGraphs.h"
#include "../Transformation/TransformGraph.h"
#include "../Utils/Params.h"
#include "../STC/DynamicPricing.h"
#include "Transformation/WedgeHyperGraph.h"
#include <unordered_set>
#include <set>
#include <map>

class DynamicPricingStreaming_STCplus {

public:

    DynamicPricingStreaming_STCplus();

    void run_update_sequence(Sigma const &sigma);

#ifdef PRINT_DATA_TO_FILES
    void save_data(std::string const &outfile);
#endif

    Cover C;
    WedgeHyperGraphDynamic W;

private:

    void update(const std::vector<WHyperEdge> &F);

    void update(const WHyperEdge &e);

    void insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv, std::pair<NodeId, NodeId> v_edge);

    void deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void decreaseWeight(std::pair<NodeId, NodeId> u, double d);

    void increaseWeight(std::pair<NodeId, NodeId> u, double d);

//    bool isTight(NodeId u);

    bool inCover(NodeId u);

    void removeFromCover(NodeId u);

    void update_counts();


    std::map<WHyperEdge, double> prices;
    // std::vector<double> price_sum;

#ifdef PRINT_DATA_TO_FILES
    std::vector<uint> C_sizes;
    std::vector<std::vector<std::pair<NodeId, NodeId>>> C_counts;
    std::vector<uint> C_weights;
    std::vector<uint> W_edges;
    uint num_time_windows = 0;
#endif
};

void run_dynamic_experiment_streaming_STCplus(const TemporalGraphStream& tgs, Params const &params);

std::pair<NodeId, NodeId> get_vedge_nodeId(std::pair<NodeId, NodeId> u, std::pair<NodeId, NodeId> v);

#endif //CPPSOURCE_DYNAMICPRICINGSTREAMING_STCPLUS_H
