#include "DynamicPricingSlowStream_STCplus.h"

#include <cassert>
#include <numeric>

#include "Utils/HelperFunctions.h"
#include "Utils/SGLog.h"
#include "STC/DynamicPricing.h"

using namespace std;


void WedgeHyperGraphDynamic2::insertVertex(std::pair<NodeId, NodeId> &wu) {
    if (edgeNodeMap.find(wu) == edgeNodeMap.end()) {
        if (unused_ids.empty()){
            edgeNodeMap[wu] = edge_index++;
            nodes.emplace_back();
            nodes.back().e = wu;
            nodes.back().id = edge_index-1;
            nodes.back().weight = 0;
        } else {
            auto nid = unused_ids.back();
            unused_ids.pop_back();
            edgeNodeMap[wu] = nid;
            nodes[nid].e = wu;
            nodes[nid].id = nid;
            nodes[nid].weight = 0;
        }
    }
}

void WedgeHyperGraphDynamic2::deleteVertex(std::pair<NodeId, NodeId> &wu) {
    if (edgeNodeMap.find(wu) != edgeNodeMap.end()) {
        auto nid = edgeNodeMap[wu];
        unused_ids.push_back(nid);
        edgeNodeMap.erase(wu);
    }
}

void WedgeHyperGraphDynamic2::addEdge(std::pair<NodeId, NodeId> wu,
                                     std::pair<NodeId, NodeId> wv, std::pair<NodeId, NodeId> v_edge) {

    insertVertex(wu);
    insertVertex(wv);
    insertVertex(v_edge);

    auto u = edgeNodeMap[wu];
    auto v = edgeNodeMap[wv];
    auto w = edgeNodeMap[v_edge];

    WHyperEdge we = order(u, v, w);

    edges.insert(we);

    if (nodes[w].weight == 0) nodes[w].weight = alpha;
}

void WedgeHyperGraphDynamic2::removeEdge(const NodeId u, const NodeId v, const NodeId w) {
    const auto e = order(u, v, w);
    edges.erase(e);
}

//////////////////////////////////////////

pair<NodeId, NodeId> get_vedge_nodeId1(pair<NodeId, NodeId> u, pair<NodeId, NodeId> v) {
    pair<NodeId, NodeId> v_edge;
    if (u.first == v.first) {
        v_edge = order(u.second, v.second);
    } else if (u.first == v.second) {
        v_edge = order(u.second, v.first);
    } else if (u.second == v.first) {
        v_edge = order(u.first, v.second);
    } else if (u.second == v.second) {
        v_edge = order(u.first, v.first);
    }
    return v_edge;
}

void DynamicPricingSlowStream_STCplus::run_update_sequence(Sigma const &sigma) {
    if (sigma.empty()) return;
    for (auto &s : sigma) {
        auto u = order(s.wu.first, s.wu.second);
        auto v = order(s.wv.first, s.wv.second);
        switch (s.updateType) {
            case IE: {
                pair<NodeId, NodeId> v_edge = get_vedge_nodeId1(u, v);
                insertEdge(u, v, v_edge);
                break;
            }
            case DE: {
                deleteEdge(u, v);
                break;
            }
            case CW: {
                assert(s.d != 0);
                if (s.d > 0) increaseWeight(u, s.d);
                else decreaseWeight(u, s.d);
                break;
            }
            case RN: {
                auto ordered_u = order(u.first, u.second);
                auto node = W.edgeNodeMap[ordered_u];
                // assert(W.nodes[node].neighbors.empty());
                W.nodes[node].weight = 0;
                W.deleteVertex(ordered_u);
                break;
            }
        }
    }
    update();
    // cout << W.num_edges << endl;
#ifdef PRINT_DATA_TO_FILES
    update_counts();
#endif
}

void DynamicPricingSlowStream_STCplus::update() {
    C.clear();
    price_sum.resize(W.nodes.size());

    for (auto &e : W.edges) {
        auto const u = e.u;
        auto const v = e.v;
        auto const w = e.w;

            if (C.find(u) != C.end() || C.find(v) != C.end() || C.find(w) != C.end()) {continue;}

            double const wu = W.nodes[u].weight - price_sum[u];
            double const wv = W.nodes[v].weight - price_sum[v];
            double const ww = W.nodes[w].weight - price_sum[w];;


            const double increase = min({wu, wv, ww});
            price_sum[u] += increase;
            price_sum[v] += increase;
            price_sum[w] += increase;

            if (W.nodes[u].isTight()) C.insert(u);
            if (W.nodes[v].isTight()) C.insert(v);
            if (W.nodes[w].isTight()) C.insert(w);
        }
}

void DynamicPricingSlowStream_STCplus::insertEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv,
                                                  pair<NodeId, NodeId> v_edge) {
    W.addEdge(wu, wv, v_edge);
}

void DynamicPricingSlowStream_STCplus::deleteEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv) {
    NodeId u = W.edgeNodeMap[wu], v = W.edgeNodeMap[wv];
    pair<NodeId, NodeId> v_edge = get_vedge_nodeId1(wu, wv);
    NodeId w = W.edgeNodeMap[v_edge];
    W.removeEdge(u, v, w);
}

void DynamicPricingSlowStream_STCplus::decreaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    auto u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
}

void DynamicPricingSlowStream_STCplus::increaseWeight(std::pair<NodeId, NodeId> wu, double d) {
    W.insertVertex(wu);
    auto u = W.edgeNodeMap[wu];
    W.nodes[u].weight += d;
}


void DynamicPricingSlowStream_STCplus::update_counts() {
    C_sizes.push_back(C.size());
    C_weights.push_back(0);
    for (auto &n : W.nodes) {
        if (C.find(n.id) != C.end()) {
//            C_counts[n.id]++;
            C_weights.back() += n.weight;
        }
    }
    num_time_windows++;
    W_edges.push_back(W.edges.size());
    C_counts.emplace_back();
    for (auto &c : C) {
        C_counts.back().push_back(W.nodes[c].e);
    }
}

void DynamicPricingSlowStream_STCplus::save_data(const string &outfile) {
    auto counts_file = outfile + "_stcplus.ccounts_slow";
    auto sizes_file = outfile + "_stcplus.csizes_slow";
    auto wedges_file = outfile + "_stcplus.wedges_slow";
    auto weights_file = outfile + "_stcplus.cweights_slow";
//    vector<string> normalized;
//    for (uint i = 0; i < C_counts.size(); ++i) {
//        auto &v = C_counts[i];
//        double val = (double) v / num_time_windows;
//        string s = to_string(W.nodes[i].e.first) + " " + to_string(W.nodes[i].e.second) + " " + to_string(val);
//        normalized.push_back(s);
//    }
    vector<string> normalized;
    normalized.push_back(to_string(C_counts.size()));
    for (auto &Cv : C_counts) {
        for (auto &p : Cv) {
            string s = to_string(p.first) + " " + to_string(p.second);
            normalized.push_back(s);
        }
        normalized.emplace_back("\n");
    }
    HF::writeVectorToFile(counts_file, normalized);
    HF::writeVectorToFile(sizes_file, C_sizes);
    HF::writeVectorToFile(weights_file, C_weights);
    HF::writeVectorToFile(wedges_file, W_edges);
}



void run_dynamic_experiment_slow_stream_STCplus(const TemporalGraphStream& tgs, Params const &params) {

    auto delta = params.delta;
    auto stepsize = params.stepsize;

    uint pos = params.start_pos;

    Time start = tgs.edges[pos].t;
    Time end = tgs.edges.back().t;

    uint cur_start = start,
            cur_end = start + delta - 1;

    DynamicPricingSlowStream_STCplus dynamic_MWVC;

    list<TemporalEdge> edges;

    Graph graph(tgs.num_nodes);

#ifdef PRINT_TIMES
    Timer timer;
    vector<double> times;
    vector<uint> num_edges;
    vector<uint> num_nodes;
    vector<uint> C_size;

    double time = 0;
#endif
    uint po = 0;

    while (tgs.edges[pos].t <= end - delta + 1) {

        TemporalEdges new_edges, removed_edges;

        if (!edges.empty()) {
            int a = tgs.edges[pos].t - cur_end;
            int b = edges.front().t - cur_start;
            int c = min(a, b);
            if (c > 0) {
                cur_start += c;
                cur_end += c;
            }
        }

        auto it = edges.begin();
        while (it != edges.end()) {
            if ((*it).t < cur_start) {
                removed_edges.push_back(*it);
                it = edges.erase(it);
            } else {
                break;
            }
        }

        while (tgs.edges[pos].t <= cur_end) {
            if (tgs.edges[pos].u_id != tgs.edges[pos].v_id)
                new_edges.push_back(tgs.edges[pos]);
            pos++;
            if (pos >= tgs.edges.size()) break;
        }

        cur_start += stepsize;
        cur_end = min((ulong)stepsize + cur_end, end);

        if (removed_edges.empty() && new_edges.empty()) continue;

        edges.insert(edges.end(), new_edges.begin(), new_edges.end());

        auto sigma = update_aggregation_and_wedge_graph(new_edges, removed_edges, graph);

#ifdef PRINT_TIMES
        if (po % 10000 == 0) {
            cout << "edges size:\t" << edges.size() << endl;
            cout << "new edges:\t" << new_edges.size() << endl;
            cout << "rem. edges:\t" << removed_edges.size() << endl;
            cout << "Sigma len:\t" << sigma.size() << endl;
            cout << "#A.nodes:\t" << graph.nodes.size() << endl;
            cout << "#A.edges:\t" << graph.num_edges << endl;
            cout << "#W.nodes:\t" << dynamic_MWVC.W.nodes.size() << endl;
            cout << "#W.edges:\t" << dynamic_MWVC.W.edges.size() << endl;
            cout << "#C before: " << dynamic_MWVC.C.size() << endl;
        }
        timer.start();
#endif

        dynamic_MWVC.run_update_sequence(sigma);

#ifdef PRINT_TIMES
        auto t = timer.stop();
        time += t;

        if (po % 10000 == 0) {
            cout << "Iter:\t" << po << endl;
            cout << "Time:\t" << t << endl;
            cout << "Time sum:\t" << time << endl;
            cout << "Pos: \t" << pos << " of " << tgs.edges.size() << endl;
            cout << "------------------------------------------" << endl;
        }
#endif
        po++;

    }
#ifdef PRINT_TIMES
    SGLog::log() << "running time complete: " << time << endl;
    cout << fixed;
    cout << time << endl;
#endif
#ifdef PRINT_DATA_TO_FILES
    dynamic_MWVC.save_data(params.dataset_path);
#endif
}