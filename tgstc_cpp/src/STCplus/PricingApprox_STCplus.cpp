#include "PricingApprox_STCplus.h"
#include "STC/UnweightedApprox.h"

using namespace std;

unordered_set<NodeId> pricing_approx_STCplus(WedgeHyperGraph const &w) {

    vector<double> vertex_price_sum(w.nodes.size(), 0);

    unordered_set<NodeId> cover;

    vector<pair<WHyperEdge, uint>> perm;
    for (auto &e : w.edges) {
        auto &u = w.nodes[e.u];
        auto &v = w.nodes[e.v];
        auto &x = w.nodes[e.w];
//        auto weight1 = u.weight + v.weight + x.weight;
        auto weight2 = u.neighbors.size() + v.neighbors.size() + x.neighbors.size();
        perm.emplace_back(e, weight2);
    }

    sort(perm.begin(), perm.end(), [](const auto &p1, const auto &p2)
        {return p1.second > p2.second;});

    for (auto &p : perm) {
        auto &e = p.first;
        if (vertex_price_sum[e.u] == w.nodes[e.u].weight
            || vertex_price_sum[e.v] == w.nodes[e.v].weight
            || vertex_price_sum[e.w] == w.nodes[e.w].weight) continue;

        double wu = w.nodes[e.u].weight - vertex_price_sum[e.u];
        double wv = w.nodes[e.v].weight - vertex_price_sum[e.v];
        double ww = w.nodes[e.w].weight - vertex_price_sum[e.w];

        vector<double> deltas({wu, wv, ww});
        sort(deltas.begin(), deltas.end(), less<>());
        double increase = deltas.front();

        vertex_price_sum[e.u] += increase;
        vertex_price_sum[e.v] += increase;
        vertex_price_sum[e.w] += increase;

        if (vertex_price_sum[e.u] == w.nodes[e.u].weight) cover.insert(e.u);
        if (vertex_price_sum[e.v] == w.nodes[e.v].weight) cover.insert(e.v);
        if (vertex_price_sum[e.w] == w.nodes[e.w].weight) cover.insert(e.w);

    }

    unordered_set<NodeId> result;
    uint number_of_virtual_nodes = 0;
    for (auto &v : cover) {
        if (!w.nodes[v].is_virtual)
            result.insert(v);
        else
            number_of_virtual_nodes++;
    }
    cout << "Number of virtual nodes: " << number_of_virtual_nodes << endl;

    return result;
}

unordered_set<NodeId> pricing_approx_STCplus2(WedgeHyperGraph const &w, Graph const &g) {

    vector<double> vertex_price_sum(w.nodes.size(), 0);
    map<pair<NodeId,NodeId>, uint> common_neighbors = get_common_neighbors(g); // number of triangles

    unordered_set<NodeId> cover;

    vector<pair<WHyperEdge, uint>> perm;
    for (auto &e : w.edges) {
        auto &u = w.nodes[e.u];
        auto &v = w.nodes[e.v];
        auto &x = w.nodes[e.w];

//        double wu = 0, wv = 0, ww = 0;
//        if (!u.is_virtual) wu = u.weight;
//        if (!v.is_virtual) wv = v.weight;
//        if (!x.is_virtual) ww = x.weight;

        uint wu = 0, wv = 0, ww = 0;
        if (!u.is_virtual) wu = u.neighbors.size();
        if (!v.is_virtual) wv = v.neighbors.size();
        if (!x.is_virtual) ww = x.neighbors.size();

        auto weight2 = wu + wv + ww;
        perm.emplace_back(e, weight2);
    }

    sort(perm.begin(), perm.end(), [](const auto &p1, const auto &p2)
    {
        return p1.second > p2.second;
    });

    for (auto &p : perm) {
        auto &e = p.first;
        if (vertex_price_sum[e.u] >= w.nodes[e.u].weight - 0.00001
            || vertex_price_sum[e.v] >= w.nodes[e.v].weight - 0.00001
            || vertex_price_sum[e.w] >= w.nodes[e.w].weight - 0.00001) continue;

        double wu = w.nodes[e.u].weight - vertex_price_sum[e.u];
        double wv = w.nodes[e.v].weight - vertex_price_sum[e.v];
        double ww = w.nodes[e.w].weight - vertex_price_sum[e.w];

        vector<double> deltas({wu, wv, ww});
        sort(deltas.begin(), deltas.end(), less<>());
        double increase = deltas.front();

        vertex_price_sum[e.u] += increase;
        vertex_price_sum[e.v] += increase;
        vertex_price_sum[e.w] += increase;

        if (vertex_price_sum[e.u] >= w.nodes[e.u].weight - 0.00001) cover.insert(e.u);
        if (vertex_price_sum[e.v] >= w.nodes[e.v].weight - 0.00001) cover.insert(e.v);
        if (vertex_price_sum[e.w] >= w.nodes[e.w].weight - 0.00001) cover.insert(e.w);

    }

    unordered_set<NodeId> result;
    uint number_of_virtual_nodes = 0;
    for (auto &v : cover) {
        if (!w.nodes[v].is_virtual)
            result.insert(v);
        else
            number_of_virtual_nodes++;
    }
    cout << "Number of virtual nodes: " << number_of_virtual_nodes << endl;

    return result;
}