#ifndef CPPSOURCE_PRICINGAPPROX_STCPLUS_H
#define CPPSOURCE_PRICINGAPPROX_STCPLUS_H

#include "../Transformation/TransformGraph.h"
#include "../Transformation/WedgeHyperGraph.h"

std::unordered_set<NodeId> pricing_approx_STCplus(WedgeHyperGraph const &w);
std::unordered_set<NodeId> pricing_approx_STCplus2(WedgeHyperGraph const &w, Graph const &g);

#endif //CPPSOURCE_PRICINGAPPROX_STCPLUS_H
