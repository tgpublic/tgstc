#include "UnweightedApproximation_STCplus.h"
#include "STC/UnweightedApprox.h"

using namespace std;


std::unordered_set<NodeId> unweighted_approx_matching_STCplus(WedgeHyperGraph const &w1) {
    WedgeHyperGraph w = w1;

    unordered_set<NodeId> matching;

    set<pair<long, WHyperEdge>, greater<>> edegs;
    for (auto &e : w.edges) {
        edegs.insert({(w.nodes[e.u].neighbors.size()+w.nodes[e.v].neighbors.size()+w.nodes[e.w].neighbors.size()), e});
    }

    while (!edegs.empty() && w.num_edges > 0) {
        auto next = *edegs.begin();
        auto e = next.second;
        edegs.erase(next);

        w.removeEdge(e.u, e.v, e.w);

        if (matching.find(e.u) == matching.end() && matching.find(e.v) == matching.end()
            && matching.find(e.w) == matching.end()) {
            matching.insert(e.u);
            matching.insert(e.v);
            matching.insert(e.w);
        }
    }

    unordered_set<NodeId> matching_result;
    uint number_of_virtual_nodes = 0;
    for (auto &v : matching) {
        if (!w.nodes[v].is_virtual)
            matching_result.insert(v);
        else
            number_of_virtual_nodes++;
    }

    cout << "Number of virtual nodes: " << number_of_virtual_nodes << endl;
    return matching_result;
}


//std::unordered_set<NodeId> unweighted_approx_matching_STCplus_hd2(WedgeHyperGraph const &w1, Graph const &g) {
//    WedgeHyperGraph w = w1;
//
//    unordered_set<NodeId> matching;
//
//    vector<pair<long, WHyperEdge>> edegs;
//    for (auto &e : w.edges) {
//        edegs.emplace_back((w.nodes[e.u].neighbors.size()+w.nodes[e.v].neighbors.size()+w.nodes[e.w].neighbors.size()), e);
//    }
//    sort(edegs.begin(), edegs.end(), less<>());
//    map<pair<NodeId,NodeId>, uint> common_neighbors = get_common_neighbors(g); // number of triangles
//
//    long count = 0;
//    while (!edegs.empty() && w.num_edges > 0) {
//        auto next = edegs.back();
//        auto e = next.second;
//        edegs.pop_back();
//
////        auto m = next.first;
////        vector<WHyperEdge> equaldegree;
////        for (auto &p : edegs) {
////            if (p.first == m) {
////                equaldegree.push_back(p.second);
////            }
////        }
////        ulong maxeqdeg = MAX_ULONG_VALUE;
////        for (auto &r : equaldegree) {
////            uint cu = 0, cv = 0, cw = 0;
////            if (!w.nodes[r.u].is_virtual && common_neighbors.find(w.nodes[r.u].e) != common_neighbors.end())
////                cu = common_neighbors.at(w.nodes[r.u].e);
////            if (!w.nodes[r.v].is_virtual && common_neighbors.find(w.nodes[r.v].e) != common_neighbors.end())
////                cv = common_neighbors.at(w.nodes[r.v].e);
////            if (!w.nodes[r.w].is_virtual && common_neighbors.find(w.nodes[r.w].e) != common_neighbors.end())
////                cw = common_neighbors.at(w.nodes[r.w].e);
////
////            if (cu + cv + cw < maxeqdeg) {
////                maxeqdeg = cu + cv + cw;
////                e = r;
////            }
////        }
//
//        if (matching.find(e.u) == matching.end() && matching.find(e.v) == matching.end()
//            && matching.find(e.w) == matching.end()) {
//            matching.insert(e.u);
//            matching.insert(e.v);
//            matching.insert(e.w);
//
//            w.removeIncidentEdges(e.u);
//            w.removeIncidentEdges(e.v);
//            w.removeIncidentEdges(e.w);
//        } /*else {
//            if (matching.find(e.u) != matching.end())
//                w.removeIncidentEdges(e.u);
//            if (matching.find(e.v) != matching.end())
//                w.removeIncidentEdges(e.v);
//            if (matching.find(e.w) != matching.end())
//                w.removeIncidentEdges(e.w);
//        }*/
//
//
//        if (count % 1000 == 0) {
//            cout << w.num_edges << " " << next.first << endl;
//
//            edegs.clear();
//            for (auto &f: w.edges) {
//                edegs.emplace_back((w.nodes[f.u].neighbors.size() +
//                                    w.nodes[f.v].neighbors.size() + w.nodes[f.w].neighbors.size()), f);
//            }
//            sort(edegs.begin(), edegs.end(), less<>());
//        }
//        count++;
//    }
//
//    unordered_set<NodeId> matching_result;
//    uint number_of_virtual_nodes = 0;
//    for (auto &v : matching) {
//        if (!w.nodes[v].is_virtual)
//            matching_result.insert(v);
//        else
//            number_of_virtual_nodes++;
//    }
//
//    cout << "Number of virtual nodes: " << number_of_virtual_nodes << endl;
//    return matching_result;
//}

std::unordered_set<NodeId> unweighted_approx_matching_STCplus_hd2(WedgeHyperGraph const &w1, Graph const &g) {
    WedgeHyperGraph w = w1;

    unordered_set<NodeId> matching;

    vector<pair<long, WHyperEdge>> edegs;
    for (auto &e : w.edges) {
        edegs.emplace_back((w.nodes[e.u].neighbors.size()+w.nodes[e.v].neighbors.size()+w.nodes[e.w].neighbors.size()), e);
    }
    sort(edegs.begin(), edegs.end(), less<>());
    map<pair<NodeId,NodeId>, uint> common_neighbors = get_common_neighbors(g); // number of triangles

    long count = 0;
    while (!edegs.empty() && w.num_edges > 0) {
        auto next = edegs.back();
        auto e = next.second;

        auto m = next.first;
        vector<pair<WHyperEdge, EdgeId>> equaldegree;
        EdgeId eid = 0;
        for (auto &p : edegs) {
            if (p.first == m) {
                equaldegree.emplace_back(p.second, eid);
            }
            eid++;
        }
        ulong maxeqdeg = MAX_ULONG_VALUE;
        EdgeId oldeid = UINT32_MAX;
        for (auto &r : equaldegree) {
            uint cu = 0, cv = 0, cw = 0;
            if (!w.nodes[r.first.u].is_virtual && common_neighbors.find(w.nodes[r.first.u].e) != common_neighbors.end())
                cu = common_neighbors.at(w.nodes[r.first.u].e);
            if (!w.nodes[r.first.v].is_virtual && common_neighbors.find(w.nodes[r.first.v].e) != common_neighbors.end())
                cv = common_neighbors.at(w.nodes[r.first.v].e);
            if (!w.nodes[r.first.w].is_virtual && common_neighbors.find(w.nodes[r.first.w].e) != common_neighbors.end())
                cw = common_neighbors.at(w.nodes[r.first.w].e);

            if (cu + cv + cw < maxeqdeg) {
                maxeqdeg = cu + cv + cw;
                e = r.first;
                oldeid = r.second;
            }
        }

        edegs[oldeid] = edegs.back();
        edegs.pop_back();

        if (matching.find(e.u) == matching.end() && matching.find(e.v) == matching.end()
            && matching.find(e.w) == matching.end()) {
            matching.insert(e.u);
            matching.insert(e.v);
            matching.insert(e.w);

            w.removeIncidentEdges(e.u);
            w.removeIncidentEdges(e.v);
            w.removeIncidentEdges(e.w);
        }

        if (count % 1000 == 0) {
//            cout << w.num_edges << " " << next.first << endl;

            edegs.clear();
            for (auto &f: w.edges) {
                edegs.emplace_back((w.nodes[f.u].neighbors.size() +
                                    w.nodes[f.v].neighbors.size() + w.nodes[f.w].neighbors.size()), f);
            }
            sort(edegs.begin(), edegs.end(), less<>());
        }
        count++;
    }

    unordered_set<NodeId> matching_result;
    uint number_of_virtual_nodes = 0;
    for (auto &v : matching) {
        if (!w.nodes[v].is_virtual)
            matching_result.insert(v);
        else
            number_of_virtual_nodes++;
    }

    cout << "Number of virtual nodes: " << number_of_virtual_nodes << endl;
    return matching_result;
}

std::unordered_set<NodeId> unweighted_approx_matching_STCplus_hd(WedgeHyperGraph const &w1, Graph const &g) {
    WedgeHyperGraph w = w1;

    unordered_set<NodeId> matching;

    vector<WHyperEdge> equaldegree;
    uint cur_max_deg = 0;
    for (auto &e : w.edges) {
        auto cd = w.nodes[e.u].neighbors.size()+w.nodes[e.v].neighbors.size()+w.nodes[e.w].neighbors.size();
        if (cd == cur_max_deg) {
            equaldegree.push_back(e);
        }
        if (cd > cur_max_deg) {
            cur_max_deg = cd;
            equaldegree.clear();
            equaldegree.push_back(e);
        }
    }
    map<pair<NodeId,NodeId>, uint> common_neighbors = get_common_neighbors(g); // number of triangles

    while (w.num_edges > 0) {
        cout << w.num_edges << endl;
        WHyperEdge e = equaldegree.front();
        ulong maxeqdeg = MAX_ULONG_VALUE;
        for (auto &r : equaldegree) {
            uint cu = 0, cv = 0, cw = 0;
            if (!w.nodes[r.u].is_virtual && common_neighbors.find(w.nodes[r.u].e) != common_neighbors.end())
                cu = common_neighbors.at(w.nodes[r.u].e);
            if (!w.nodes[r.v].is_virtual && common_neighbors.find(w.nodes[r.v].e) != common_neighbors.end())
                cv = common_neighbors.at(w.nodes[r.v].e);
            if (!w.nodes[r.w].is_virtual && common_neighbors.find(w.nodes[r.w].e) != common_neighbors.end())
                cw = common_neighbors.at(w.nodes[r.w].e);

            if (cu + cv + cw < maxeqdeg) {
                maxeqdeg = cu + cv + cw;
                e = r;
            }
        }

//        cout << e.u << " " << e.v << " " << e.w << " " << maxeqdeg << endl;

        if (matching.find(e.u) == matching.end() && matching.find(e.v) == matching.end()
            && matching.find(e.w) == matching.end()) {
            matching.insert(e.u);
            matching.insert(e.v);
            matching.insert(e.w);

            w.removeIncidentEdges(e.u);
            w.removeIncidentEdges(e.v);
            w.removeIncidentEdges(e.w);
        }

        equaldegree.clear();
        cur_max_deg = 0;
        for (auto &f : w.edges) {
            auto cd = w.nodes[f.u].neighbors.size()+w.nodes[f.v].neighbors.size()+w.nodes[f.w].neighbors.size();
            if (cd == cur_max_deg) {
                equaldegree.push_back(f);
            }
            if (cd > cur_max_deg) {
                cur_max_deg = cd;
                equaldegree.clear();
                equaldegree.push_back(f);
            }
        }
//        sort(equaldegree.begin(), equaldegree.end(), [](auto a, auto b){return tie(a.u,a.v,a.w) > tie(b.u, b.v, b.w);});

    }

    unordered_set<NodeId> matching_result;
    uint number_of_virtual_nodes = 0;
    for (auto &v : matching) {
        if (!w.nodes[v].is_virtual)
            matching_result.insert(v);
        else
            number_of_virtual_nodes++;
    }

    cout << "Number of virtual nodes: " << number_of_virtual_nodes << endl;
    return matching_result;
}




std::unordered_set<NodeId> unweighted_approx_highdeg(WedgeHyperGraph const &w) {
    unordered_set<NodeId> cover;
    set<pair<uint, NodeId>, std::greater<>> node_degrees;
    vector<uint> degrees(w.nodes.size(), 0);
    for (auto &n : w.nodes) {
        node_degrees.insert({n.neighbors.size(), n.id});
        degrees[n.id] = n.neighbors.size();
    }
    while (!node_degrees.empty()) {
        auto next_it = node_degrees.begin();
        node_degrees.erase(next_it);
        if (degrees[(*next_it).second]==0) continue;
        NodeId nid = (*next_it).second;
        cover.insert(nid);
        degrees[nid] = 0;
        for (auto &v : w.nodes[nid].neighbors) {
            if (degrees[v.first] > 0) degrees[v.first]--;
            if (degrees[v.second] > 0) degrees[v.second]--;
            node_degrees.insert({w.nodes[v.first].neighbors.size(), v.first});
            node_degrees.insert({w.nodes[v.second].neighbors.size(), v.second});
        }
    }

    unordered_set<NodeId> matching_result;
    for (auto &v : cover) {
        if (!w.nodes[v].is_virtual)
            matching_result.insert(v);
    }

    return matching_result;
}

