//#include <cassert>
//#include <numeric>
//#include "DynamicPricingSlowStream_STCplus.h"
//
//#include "DynamicPricingStreaming_STCplus.h"
//#include "Utils/HelperFunctions.h"
//#include "Utils/SGLog.h"
//#include "STC/DynamicPricing.h"
//
//using namespace std;
//
//
//void DynamicPricingSlowStream_STCplus::run_update_sequence(Sigma const &sigma) {
//    if (sigma.empty()) return;
//    for (auto &s : sigma) {
//        auto u = order(s.wu.first, s.wu.second);
//        auto v = order(s.wv.first, s.wv.second);
//        switch (s.updateType) {
//            case IE: {
//                pair<NodeId, NodeId> v_edge = get_vedge_nodeId(u, v);
//                insertEdge(u, v, v_edge);
//                break;
//            }
//            case DE: {
//                deleteEdge(u, v);
//                break;
//            }
//            case CW: {
//                assert(s.d != 0);
//                if (s.d > 0) increaseWeight(u, s.d);
//                else decreaseWeight(u, s.d);
//                break;
//            }
//            case RN: {
//                auto ordered_u = order(u.first, u.second);
//                auto node = W.edgeNodeMap[ordered_u];
//                // assert(W.nodes[node].neighbors.empty());
//                W.nodes[node].weight = 0;
//                W.deleteVertex(ordered_u);
//                break;
//            }
//        }
//    }
//    update();
//    // cout << W.num_edges << endl;
//#ifdef PRINT_DATA_TO_FILES
//    update_counts();
//#endif
//}
//
//void DynamicPricingSlowStream_STCplus::update() {
//
//    // Timer timer;
//    // timer.start();
//
//    C.clear();
//    // vector<double> price_sum(W.nodes.size(), 0);
//    price_sum.resize(W.nodes.size());
//
//    // ulong num_edges = 0;
//    for (auto &u : W.nodes) {
//        for (const auto &[fst, snd]: u.neighbors) {
//              // num_edges ++;
//
//            // if (W.nodes[u.id].weight == price_sum[u.id] ||  W.nodes[fst].weight == price_sum[fst]|| W.nodes[snd].weight == price_sum[fst]) continue;
//            if (C.find(fst) == C.end() || C.find(snd) == C.end() || C.find(u.id) == C.end()) {continue;}
//            // if (W.nodes[u.id].isTight() ||  W.nodes[fst].isTight()|| W.nodes[snd].isTight()) continue;
//
//
//            double const wu = W.nodes[u.id].weight - price_sum[u.id];
//            double const wv = W.nodes[fst].weight - price_sum[fst];
//            double const ww = W.nodes[snd].weight - price_sum[snd];;
//
//
//            const double increase = min({wu, wv, ww});
//            price_sum[u.id] += increase;
//            price_sum[fst] += increase;
//            price_sum[snd] += increase;
//
//            // if (W.nodes[u.id].weight == price_sum[u.id]) C.insert(u.id);
//            // if (W.nodes[fst].weight == price_sum[fst]) C.insert(fst);
//            // if (W.nodes[snd].weight == price_sum[fst]) C.insert(snd);
//
//            if (W.nodes[u.id].isTight()) C.insert(u.id);
//            if (W.nodes[fst].isTight()) C.insert(fst);
//            if (W.nodes[snd].isTight()) C.insert(snd);
//        }
//    }
//    // cout << "> " << num_edges << endl;
//    // timer.stopAndPrintTime();
//
//}
//
//void DynamicPricingSlowStream_STCplus::insertEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv,
//                                                  pair<NodeId, NodeId> v_edge) {
//    W.addEdge(wu, wv, v_edge);
//}
//
//void DynamicPricingSlowStream_STCplus::deleteEdge(pair<NodeId, NodeId> wu, pair<NodeId, NodeId> wv) {
//    NodeId u = W.edgeNodeMap[wu], v = W.edgeNodeMap[wv];
//    pair<NodeId, NodeId> v_edge = get_vedge_nodeId(wu, wv);
//    NodeId w = W.edgeNodeMap[v_edge];
//    W.removeEdge(u, v, w);
//}
//
//void DynamicPricingSlowStream_STCplus::decreaseWeight(std::pair<NodeId, NodeId> wu, double d) {
//    auto u = W.edgeNodeMap[wu];
//    W.nodes[u].weight += d;
//}
//
//void DynamicPricingSlowStream_STCplus::increaseWeight(std::pair<NodeId, NodeId> wu, double d) {
//    W.insertVertex(wu, false);
//    auto u = W.edgeNodeMap[wu];
//    W.nodes[u].weight += d;
//}
//
//DynamicPricingSlowStream_STCplus::DynamicPricingSlowStream_STCplus() {
////    C_counts.resize(W.nodes.size(), 0);
//}
//
//void DynamicPricingSlowStream_STCplus::update_counts() {
//    C_sizes.push_back(C.size());
//    C_weights.push_back(0);
//    for (auto &n : W.nodes) {
//        if (C.find(n.id) != C.end()) {
////            C_counts[n.id]++;
//            C_weights.back() += n.weight;
//        }
//    }
//    num_time_windows++;
//    W_edges.push_back(W.num_edges);
//    C_counts.emplace_back();
//    for (auto &c : C) {
//        C_counts.back().push_back(W.nodes[c].e);
//    }
//}
//
//void DynamicPricingSlowStream_STCplus::save_data(const string &outfile) {
//    auto counts_file = outfile + "_stcplus.ccounts_slow";
//    auto sizes_file = outfile + "_stcplus.csizes_slow";
//    auto wedges_file = outfile + "_stcplus.wedges_slow";
//    auto weights_file = outfile + "_stcplus.cweights_slow";
////    vector<string> normalized;
////    for (uint i = 0; i < C_counts.size(); ++i) {
////        auto &v = C_counts[i];
////        double val = (double) v / num_time_windows;
////        string s = to_string(W.nodes[i].e.first) + " " + to_string(W.nodes[i].e.second) + " " + to_string(val);
////        normalized.push_back(s);
////    }
//    vector<string> normalized;
//    normalized.push_back(to_string(C_counts.size()));
//    for (auto &Cv : C_counts) {
//        for (auto &p : Cv) {
//            string s = to_string(p.first) + " " + to_string(p.second);
//            normalized.push_back(s);
//        }
//        normalized.emplace_back("\n");
//    }
//    HF::writeVectorToFile(counts_file, normalized);
//    HF::writeVectorToFile(sizes_file, C_sizes);
//    HF::writeVectorToFile(weights_file, C_weights);
//    HF::writeVectorToFile(wedges_file, W_edges);
//}
//
//
//
//void run_dynamic_experiment_slow_stream_STCplus(const TemporalGraphStream& tgs, Params const &params) {
//
//    auto delta = params.delta;
//    auto stepsize = params.stepsize;
//
//    uint pos = params.start_pos;
//
//    Time start = tgs.edges[pos].t;
//    Time end = tgs.edges.back().t;
//
//    uint cur_start = start,
//            cur_end = start + delta - 1;
//
//    DynamicPricingSlowStream_STCplus dynamic_MWVC;
//
////    TemporalEdges edges;
//    list<TemporalEdge> edges;
//
//    Graph graph(tgs.num_nodes);
//
//#ifdef PRINT_TIMES
//    Timer timer;
//    vector<double> times;
//    vector<uint> num_edges;
//    vector<uint> num_nodes;
//    vector<uint> C_size;
//
//    double time = 0;
//#endif
//    uint po = 0;
//
//    while (tgs.edges[pos].t <= end - delta + 1) {
//
//
//        TemporalEdges new_edges, removed_edges;
//
//        if (!edges.empty()) {
//            int a = tgs.edges[pos].t - cur_end;
//            int b = edges.front().t - cur_start;
//            int c = min(a, b);
//            if (c > 0) {
//                cur_start += c;
//                cur_end += c;
//            }
//        }
//
//        //todo make more efficient with list or remove erase
//        auto it = edges.begin();
//        while (it != edges.end()) {
//            if ((*it).t < cur_start) {
//                removed_edges.push_back(*it);
//                it = edges.erase(it);
//            } else {
//                break;
//            }
//        }
//
//        while (tgs.edges[pos].t <= cur_end) {
//            if (tgs.edges[pos].u_id != tgs.edges[pos].v_id)
//                new_edges.push_back(tgs.edges[pos]);
//            pos++;
//            if (pos >= tgs.edges.size()) break;
//        }
////        cout << pos << " " << cur_start << " " << cur_end << " " << tgs.edges[pos].t << " " << end << endl;
//
//        cur_start += stepsize;
//        cur_end = min((ulong)stepsize + cur_end, end);
//
////        edges = edges_tmp;
//
//        if (removed_edges.empty() && new_edges.empty()) continue;
//
//        edges.insert(edges.end(), new_edges.begin(), new_edges.end());
//
//        auto sigma = update_aggregation_and_wedge_graph(new_edges, removed_edges, graph);
//
//#ifdef PRINT_TIMES
//        if (po % 10000 == 0) {
//            cout << "edges size:\t" << edges.size() << endl;
//            cout << "new edges:\t" << new_edges.size() << endl;
//            cout << "rem. edges:\t" << removed_edges.size() << endl;
//            cout << "Sigma len:\t" << sigma.size() << endl;
//            cout << "#A.nodes:\t" << graph.nodes.size() << endl;
//            cout << "#A.edges:\t" << graph.num_edges << endl;
//            cout << "#W.nodes:\t" << dynamic_MWVC.W.nodes.size() << endl;
//            cout << "#W.edges:\t" << dynamic_MWVC.W.num_edges << endl;
//            cout << "#C before: " << dynamic_MWVC.C.size() << endl;
//        }
//        timer.start();
//#endif
//
//        dynamic_MWVC.run_update_sequence(sigma);
//
//#ifdef PRINT_TIMES
//        auto t = timer.stop();
//        time += t;
//
////        times.push_back(t);
////        num_edges.push_back(dynamic_MWVC.W.num_edges);
////        num_nodes.push_back(dynamic_MWVC.W.nodes.size());
////        C_size.push_back(dynamic_MWVC.C.size());
//
//
//        if (po % 10000 == 0) {
//            cout << "Iter:\t" << po << endl;
//            cout << "Time:\t" << t << endl;
//            cout << "Time sum:\t" << time << endl;
//            cout << "Pos: \t" << pos << " of " << tgs.edges.size() << endl;
//            cout << "------------------------------------------" << endl;
//        }
//#endif
//        po++;
//
//    }
//#ifdef PRINT_TIMES
////    write_results_to_file(params, times, num_edges, num_nodes, C_size);
//    SGLog::log() << "running time complete: " << time << endl;
//    cout << fixed;
//    cout << time << endl;
//#endif
//#ifdef PRINT_DATA_TO_FILES
//    dynamic_MWVC.save_data(params.dataset_path);
//#endif
//}