#ifndef CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_STCPLUS_H
#define CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_STCPLUS_H
#include "../Temporalgraph/TemporalGraphs.h"
#include "../Transformation/TransformGraph.h"
#include "Utils/Params.h"
#include "STC/DynamicPricing.h"
#include "Transformation/WedgeHyperGraph.h"
#include <unordered_set>
#include <set>
#include <map>


struct WHyperNodeDynamic2 {

    NodeId id;
    double weight = 0;
    std::pair<NodeId, NodeId> e;

    double price_sum = 0;
    [[nodiscard]] bool isTight() const {
        auto const d = weight - price_sum;
        return std::abs(d) < 1e-10;
    }

};

struct WedgeHyperGraphDynamic2 {

    void insertVertex(std::pair<NodeId, NodeId> &wu);

    void deleteVertex(std::pair<NodeId, NodeId> &wu);

    void addEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv, std::pair<NodeId, NodeId> v_edge);

    void removeEdge(NodeId u, NodeId v, NodeId w);

    std::unordered_set<WHyperEdge, WHyperEdgeHash> edges;

    std::map<std::pair<NodeId,NodeId>, NodeId> edgeNodeMap;

    std::vector<uint> unused_ids;

    uint edge_index = 0;

    std::vector<WHyperNodeDynamic2> nodes;

    double alpha = 1.0;
};

////////////////////////////////////////

class DynamicPricingSlowStream_STCplus {

public:

    void run_update_sequence(Sigma const &sigma);

    void save_data(std::string const &outfile);

    Cover C;
    WedgeHyperGraphDynamic2 W;


private:

    void update();

    void insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv, std::pair<NodeId, NodeId> v_edge);

    void deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);

    void decreaseWeight(std::pair<NodeId, NodeId> u, double d);

    void increaseWeight(std::pair<NodeId, NodeId> u, double d);

    void update_counts();

    std::vector<double> price_sum;

    // NodeId insert_node(const std::pair<NodeId, NodeId> &u);

    // NodeId nid = 0;
    // std::map<std::pair<NodeId,NodeId>, NodeId> edgeNodeMap;
    // std::map<NodeId, double> weights;
    // std::set<std::tuple<NodeId, NodeId, NodeId>> edges;
    // std::set<NodeId> nodes;

    std::vector<uint> C_sizes;
//    std::vector<uint> C_counts;
    std::vector<std::vector<std::pair<NodeId, NodeId>>> C_counts;
    std::vector<uint> C_weights;
    std::vector<uint> W_edges;
    uint num_time_windows = 0;

};

void run_dynamic_experiment_slow_stream_STCplus(const TemporalGraphStream& tgs, Params const &params);


#endif //CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_STCPLUS_H
