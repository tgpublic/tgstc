//
// Created by lutz on 12.11.22.
//

#ifndef CPPSOURCE_UNWEIGHTEDPRICING_H
#define CPPSOURCE_UNWEIGHTEDPRICING_H


#include "Temporalgraph/TemporalGraphs.h"
#include "Transformation/WedgeHyperGraph.h"
#include "Transformation/TransformGraph.h"

//std::unordered_set<NodeId> unweighted_pricing_approx_STCplus(WedgeHyperGraph const &w);

std::unordered_set<NodeId> unweighted_approx_matching_STCplus(WedgeHyperGraph const &w);
std::unordered_set<NodeId> unweighted_approx_matching_STCplus_hd(WedgeHyperGraph const &w, Graph const &g);
std::unordered_set<NodeId> unweighted_approx_matching_STCplus_hd2(WedgeHyperGraph const &w, Graph const &g);

std::unordered_set<NodeId> unweighted_approx_matching_STCplus2(WedgeHyperGraph const &w, Graph const &g);

std::unordered_set<NodeId> unweighted_approx_highdeg(WedgeHyperGraph const &w);

#endif //CPPSOURCE_UNWEIGHTEDPRICING_H
