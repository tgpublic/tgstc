//#ifndef CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_STCPLUS_H
//#define CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_STCPLUS_H
//#include "../Temporalgraph/TemporalGraphs.h"
//#include "../Transformation/TransformGraph.h"
//#include "Utils/Params.h"
//#include "STC/DynamicPricing.h"
//#include "Transformation/WedgeHyperGraph.h"
//#include <unordered_set>
//#include <set>
//#include <map>
//
//
//class DynamicPricingSlowStream_STCplus {
//
//public:
//
//    DynamicPricingSlowStream_STCplus();
//
//    void run_update_sequence(Sigma const &sigma);
//
//    void save_data(std::string const &outfile);
//
//    Cover C;
//    WedgeHyperGraphDynamic W;
//
//
//private:
//
//    void update();
//
//    void insertEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv, std::pair<NodeId, NodeId> v_edge);
//
//    void deleteEdge(std::pair<NodeId, NodeId> wu, std::pair<NodeId, NodeId> wv);
//
//    void decreaseWeight(std::pair<NodeId, NodeId> u, double d);
//
//    void increaseWeight(std::pair<NodeId, NodeId> u, double d);
//
//    void update_counts();
//
//    std::vector<double> price_sum;
//
//    // NodeId insert_node(const std::pair<NodeId, NodeId> &u);
//
//    // NodeId nid = 0;
//    // std::map<std::pair<NodeId,NodeId>, NodeId> edgeNodeMap;
//    // std::map<NodeId, double> weights;
//    // std::set<std::tuple<NodeId, NodeId, NodeId>> edges;
//    // std::set<NodeId> nodes;
//
//    std::vector<uint> C_sizes;
////    std::vector<uint> C_counts;
//    std::vector<std::vector<std::pair<NodeId, NodeId>>> C_counts;
//    std::vector<uint> C_weights;
//    std::vector<uint> W_edges;
//    uint num_time_windows = 0;
//
//};
//
//void run_dynamic_experiment_slow_stream_STCplus(const TemporalGraphStream& tgs, Params const &params);
//
//
//#endif //CPPSOURCE_DYNAMICPRICINGSLOWSTREAM_STCPLUS_H
