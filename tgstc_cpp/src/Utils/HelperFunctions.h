#ifndef CENTRALITY_HELPERFUNCTIONS_H
#define CENTRALITY_HELPERFUNCTIONS_H

#include <list>
#include <set>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include "../Temporalgraph/TemporalGraphs.h"


namespace HF {

    void show_stats(TemporalGraphStream const &tgs, bool times, bool estimate, bool exact);

    template<typename T>
    void writeVectorToFile(const std::string &filename, std::vector<T> data) {
        std::ofstream fs;
        fs.open(filename);
        if (!fs.is_open()) {
            std::cout << "Could not write data to " << filename << std::endl;
            return;
        }
        fs << std::setprecision(50);
        for (auto &d: data)
            fs << d << std::endl;

        fs.close();
    }

    template<typename T, typename R>
    void writeVectorToFile(const std::string &filename, std::vector<std::pair<T, R>> data, const std::string &sep) {
        std::ofstream fs;
        fs.open(filename);
        if (!fs.is_open()) {
            std::cout << "Could not write data to " << filename << std::endl;
            return;
        }
        fs << std::setprecision(50);
        for (auto &d: data)
            fs << d.first << sep << d.second << std::endl;

        fs.close();
    }

    std::vector<unsigned long> split_string(const std::string &s, char delim);

    std::vector<std::string> split_string_str(const std::string& s, char delim);

    void get_mean_std(std::vector<double> &values, double &mean, double &stdev);

    std::pair<std::vector<double>, std::vector<double>> get_means_stdevs(std::vector<std::vector<double>> input);

    void getFileStream(const std::string &filename, std::ifstream &fs);

}

#endif //CENTRALITY_HELPERFUNCTIONS_H
