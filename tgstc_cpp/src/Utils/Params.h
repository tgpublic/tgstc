#ifndef TGPR_PARAMS_H
#define TGPR_PARAMS_H

#include <iostream>
#include <vector>
#include <limits>

struct Params {
    std::string dataset_path;
    bool parseArgs(std::vector<std::string> args);

    uint mode = 0;

    uint delta = 1000;

    uint stepsize = 1;

    uint start_pos = 0;

    double alpha = 0.5;

    uint lines = 0;

};

void show_help();

#endif //TGPR_PARAMS_H
