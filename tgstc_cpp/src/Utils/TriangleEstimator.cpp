#include "TriangleEstimator.h"

using namespace std;

void estimateTriangleAndWedges(Graph &g, uint samples) {

    uint t = 0;
    uint w = 0;


    for (uint i = 0; i < samples; ++i) {

        auto r = rand() % g.nodes.size();

        auto n = g.nodes[r];

        for (auto &u : n.neighbors) {
            for (auto &v :n.neighbors) {
                if (u >= v) continue;

                if (g.are_neighbors(u, v))
                    t++;
                else
                    w++;
            }
        }
    }

    double wedges = (1.0*w*g.nodes.size()) / (samples);
    double triangle = (1.0*t*g.nodes.size()) / (3.0 * samples);

    cout << fixed;
    cout << "wedges est: " << wedges << endl;
    cout << "triangle est: " << triangle << endl;

}