#include "Params.h"
#include "SGLog.h"
#include "vector"

using namespace std;

void show_help() {
    cout << "run with <filename> -m=[mode] -d=[delta]\n\n";
    cout << "modes:\n";
    cout << "0:\t Compute network statistics\n";
    cout << "1:\t Transform graph to .ag and .wg\n";
    cout << "2:\t Compute vertex cover on .ag\n";
    cout << "3:\t DynAppr\n";
    cout << "4:\t STCtime\n";
    cout << "5:\t DynAppr+\n";
    cout << "6:\t STCtime+\n";
    cout << endl;
    exit(0);
}

bool Params::parseArgs(vector<string> args) {
    if (args.size() < 1) {
        return false;
    }
    for (auto & arg : args) {
        SGLog::log() << arg << " ";
    }
    SGLog::log() << endl;

    try {
        dataset_path = args[0];
    } catch (...) {
        return false;
    }

    for (size_t i = 1; i < args.size(); ++i) {
        string next = args[i];
        if (next.length() < 4) return false;
        if (next.substr(0, 3) == "-m=") {
            string valstr = next.substr(3);
            mode = stoi(valstr);
            SGLog::log() << "set mode " << mode << endl;
        }
        else if (next.substr(0, 3) == "-d=") {
            string valstr = next.substr(3);
            delta = stoi(valstr);
            SGLog::log() << "set delta " << delta << endl;
        }
        else if (next.substr(0, 3) == "-l=") {
            string valstr = next.substr(3);
            lines = stoi(valstr);
            SGLog::log() << "set lines " << lines << endl;
        }
        else if (next.substr(0, 3) == "-a=") {
            string valstr = next.substr(3);
            alpha = stod(valstr);
            SGLog::log() << "set alpha " << alpha << endl;
        }
        else {
            return false;
        }
    }
    return true;
}
