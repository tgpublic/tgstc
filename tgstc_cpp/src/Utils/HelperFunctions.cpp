#include <numeric>
#include <cmath>
#include "HelperFunctions.h"
#include "SGLog.h"
#include "Transformation/TransformGraph.h"
#include "TriangleEstimator.h"

using namespace std;

namespace HF {

    std::vector<unsigned long> split_string(const std::string& s, char delim) {
        std::vector<unsigned long> result;
        std::stringstream ss(s);
        while (ss.good()) {
            std::string substr;
            getline(ss, substr, delim);
            result.push_back(stoul(substr));
        }
        return result;
    }

    std::vector<string> split_string_str(const std::string& s, char delim) {
        std::vector<string> result;
        std::stringstream ss(s);
        while (ss.good()) {
            std::string substr;
            getline(ss, substr, delim);
            result.push_back(substr);
        }
        return result;
    }

    void get_mean_std(std::vector<double> &values, double &mean, double &stdev) {
        double sum = std::accumulate(std::begin(values), std::end(values), 0.0);
        mean = sum / values.size();
        double accum = 0.0;
        std::for_each (std::begin(values), std::end(values), [&](const double d) {
            accum += (d - mean) * (d - mean);
        });
        stdev = sqrt(accum / double(values.size()));
    }

    std::pair<std::vector<double>, std::vector<double>> get_means_stdevs(std::vector<std::vector<double>> input) {
        std::vector<double> means, stdevs;
        double n_sr = sqrt(input.size());
        for (size_t i = 0; i < input.front().size(); ++i) {
            vector<double> vals;
            vals.reserve(input.size());
            for (auto & j : input) {
                vals.push_back(j.at(i));
            }
            double mean = 0;
            double stdev = 0;
            HF::get_mean_std(vals, mean, stdev);
            double err = stdev / n_sr;
            means.push_back(mean);
            stdevs.push_back(err);
        }
        return {means, stdevs};
    }

    void getFileStream(const string& filename, ifstream &fs) {
        fs.open(filename);
        if (!fs.is_open()) {
            cout << "Could not open data set " << filename << endl;
            exit(EXIT_FAILURE);
        }
    }


    void show_stats(TemporalGraphStream const &tgs, bool time, bool estimate, bool exact) {
        if (time) {
            unordered_set<Time> times;
            for (auto &e: tgs.edges)
                times.insert(e.t);
            SGLog::log() << "time int.: [" << tgs.edges.front().t << ", " << tgs.edges.back().t << "]" << endl;
            SGLog::log() << "#tgs times: " << times.size() << endl;
        }
        auto g = temporalGraphToGraph(tgs);
        SGLog::log() << "#ag nodes: " << g.nodes.size() << endl;
        SGLog::log() << "#ag edges: " << g.num_edges << endl;
        if (exact) {
            auto r = count_wedges_triangles(g);
            SGLog::log() << "#wedges: " << r.first << endl;
            SGLog::log() << "#triangles: " << r.second << endl;
        }
        if (estimate) {
            estimateTriangleAndWedges(g, 100000);
        }
    }

}


