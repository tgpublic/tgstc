#ifndef CPPSOURCE_TRIANGLEESTIMATOR_H
#define CPPSOURCE_TRIANGLEESTIMATOR_H


#include "Transformation/TransformGraph.h"

void estimateTriangleAndWedges(Graph &g, uint samples);


#endif //CPPSOURCE_TRIANGLEESTIMATOR_H
