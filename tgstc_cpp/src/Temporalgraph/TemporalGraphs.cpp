#include "TemporalGraphs.h"
#include <string>

using namespace std;

void TemporalGraphStream::sort_edges() {
    std::sort(edges.begin(), edges.end(), [](const TemporalEdge & a, const TemporalEdge & b) -> bool { return a.t < b.t; });
}


std::ostream& operator<<(std::ostream& os, const TemporalGraphStream& tgs) {
    std::string s1 = "num_nodes\t" + std::to_string(tgs.num_nodes) + "\n";
    std::string s2 = "num_edges\t" + std::to_string(tgs.edges.size()) + "\n";
    std::string s3 = "max_time\t" + std::to_string(tgs.get_max_time()) + "\n";

    return os << s1 << s2 << s3;
}

std::ostream& operator<<(std::ostream& os, const TemporalEdge &tgs) {
    std::string s1 = std::to_string(tgs.u_id) + " " + std::to_string(tgs.v_id) + " " + std::to_string(tgs.t) + "\n";

    return os << s1;
}

bool operator <(const TemporalEdge& x, const TemporalEdge& y) {
    return std::tie(x.t, x.u_id, x.v_id) < std::tie(y.t, y.u_id, y.v_id);
}