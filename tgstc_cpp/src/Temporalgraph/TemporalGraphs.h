#ifndef TGKERNEL_GRAPHDATALOADER_H
#define TGKERNEL_GRAPHDATALOADER_H

#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <unordered_set>
#include <unordered_map>

using NodeId = unsigned int;
using EdgeId = unsigned int;
using Time = unsigned long;
using Cost = float;
using Label = long;
using Labels = std::vector<Label>;

struct TemporalEdge;
struct TGNode;
using AdjacenceList = std::vector<TemporalEdge>;

static constexpr auto MAX_UINT_VALUE = std::numeric_limits<int>::max();
static constexpr auto MAX_ULONG_VALUE = std::numeric_limits<ulong>::max();

struct NodeIdManager {

    NodeIdManager() : nid(0){};

    std::unordered_map<NodeId, NodeId> nidmap;

    NodeId getNodeId(NodeId id) {
        NodeId r;
        if (nidmap.find(id) == nidmap.end()) {
            r = nid;
            nidmap.emplace(id, nid++);
        } else {
            r = nidmap.at(id);
        }
        return r;
    }

    NodeId nid = 0;
};


struct TemporalEdge {
    NodeId u_id{}, v_id{};
    Time t{};
    EdgeId id{};

    std::vector<int> values;
    std::vector<double> rvalues;

};


using TemporalEdges = std::vector<TemporalEdge>;

struct TemporalGraphStream;

struct TemporalGraphStream {
    unsigned long num_nodes;
    TemporalEdges edges;

    Time get_max_time() const {
        if (edges.empty()) return 0;
        return edges.back().t;
    }

    void sort_edges();

    Labels node_labels;

    std::unordered_map<NodeId, NodeId> nidmap;
    std::unordered_map<NodeId, NodeId> reverse_nidmap;
};


std::ostream& operator<<(std::ostream& os, const TemporalGraphStream& tgs);
std::ostream& operator<<(std::ostream& os, const TemporalEdge &tgs);
bool operator <(const TemporalEdge& x, const TemporalEdge& y);


#endif //TGKERNEL_GRAPHDATALOADER_H
