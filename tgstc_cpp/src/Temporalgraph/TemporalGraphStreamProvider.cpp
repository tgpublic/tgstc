#include "TemporalGraphStreamProvider.h"
#include "../Utils/HelperFunctions.h"
#include "Utils/Params.h"
#include <unordered_map>

using namespace std;

void TemporalGraphStreamProvider::loadTemporalGraph(const std::string& path, uint lines) {
    ifstream fs;
    HF::getFileStream(path, fs);
    string line;
    getline(fs, line);
    unsigned long num_nodes = stoul(line);
    TemporalEdges tes;
    EdgeId eid = 0;
    bool needs_sorting = false;
    Time last_time = 0;

    set<TemporalEdge> edges;

    Time ct = 0;
    uint ct1 = 0;
    uint ct2 = 20;

    while (getline(fs, line)) {

        if (line.empty()) continue;
        vector<unsigned long> l = HF::split_string(line, ' ');
        NodeId u = l[0];
        NodeId v = l[1];
        Time t = l[2];

        // if (t == ct) {
        //     ct1++;
        //     if (ct1 >= ct2) continue;
        // } else {
        //     ct1 = 0;
        //     ct = t;
        // }

        if (!needs_sorting && t > last_time) needs_sorting = true;
        else last_time = t;

        if (u > v) {
            swap(u, v);
        }

        TemporalEdge e1{u, v, t, eid++};
        if (edges.find(e1) == edges.end()) {
            edges.insert(e1);
        }


        // if (lines == edges.size()) {break;}

    }
    fs.close();
    for (auto &e : edges) {
        tgs.edges.push_back(e);
    }

    tgs.num_nodes = num_nodes;
}



//void TemporalGraphStreamProvider::loadTemporalGraph(const std::string& path, uint lines) {
//    ifstream fs;
//    HF::getFileStream(path, fs);
//    string line;
//    getline(fs, line);
//    unsigned long num_nodes = stoul(line);
//    TemporalEdges tes;
//    EdgeId eid = 0;
//    bool needs_sorting = false;
//    Time last_time = 0;
//
//    ulong readlines = MAX_ULONG_VALUE;
//    if (lines > 0) {
//        readlines = lines;
//    }
//    ulong processed_lines = 0;
//
//    unordered_set<string> edges;
//    while (getline(fs, line) && processed_lines++ < readlines) {
//        if (line.empty()) continue;
//        vector<unsigned long> l = HF::split_string(line, ' ');
//        NodeId u = l[0];
//        NodeId v = l[1];
//        Time t = l[2];
//        if (!needs_sorting && t > last_time) needs_sorting = true;
//        else last_time = t;
//
//        Time tt = 1;
//
//        if (u > v) {
//            swap(u, v);
//        }
//
//        TemporalEdge e1 = TemporalEdge(u, v, t, 0, tt, eid++);
//        tgs.edges.push_back(e1);
//
//    }
//    fs.close();
//
//    if (needs_sorting) tgs.sort_edges();
//
//    tgs.num_nodes = num_nodes;
//}


void makeSubgraph(const string &infile, const string &outfile, Time maxTime) {

    ifstream fs;
    HF::getFileStream(infile, fs);
    string line;
    TemporalEdges tes;
    EdgeId eid = 0;
    NodeIdManager nidman;

    unordered_set<string> edges;
    TemporalGraphStream tgs;
    set<Time> times;
    while (getline(fs, line)) {
        if (line.empty()) continue;
        vector<unsigned long> l = HF::split_string(line, ' ');
        NodeId u = nidman.getNodeId(l[0]);
        NodeId v = nidman.getNodeId(l[1]);
        Time t = l[2];
        times.insert(t);
        if (times.size() > maxTime) break;

        Time tt = 1;

        if (u > v) {
            swap(u, v);
        }

        TemporalEdge e1{u, v, t, eid++};
        tgs.edges.push_back(e1);

    }
    fs.close();
    tgs.num_nodes = nidman.nid;

    std::ofstream out;
    out.open(outfile);
    if (!out.is_open()) {
        std::cout << "Could not write data to " << outfile << std::endl;
        return;
    }
    out << tgs.num_nodes << endl;
    for (auto &e : tgs.edges) {
        out << e.u_id << " " << e.v_id << " " << e.t << " 1" << std::endl;
    }

    fs.close();
}
