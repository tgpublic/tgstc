#ifndef CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H
#define CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H

#include <cassert>
#include "TemporalGraphs.h"
#include "Utils/Params.h"

class TemporalGraphStreamProvider {

public:

    void loadTemporalGraph(const std::string& path, uint lines);

    TemporalGraphStream& getTGS() { return tgs; }

private:

    TemporalGraphStream tgs;

};

void makeSubgraph(const std::string &infile, const std::string &outfile, Time maxTime);


#endif //CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H
